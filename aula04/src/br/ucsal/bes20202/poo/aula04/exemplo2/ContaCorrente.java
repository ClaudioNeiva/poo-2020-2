package br.ucsal.bes20202.poo.aula04.exemplo2;

public class ContaCorrente {

	Integer numero;

	String nomeCorrentista;

	Double saldo;

	Double consultarSaldo() {
		return saldo;
	}

	void sacar(Double valor) {
		saldo -= valor;
	}

	void depositar(Double valor) {
		saldo += valor;
	}

}
