package br.ucsal.bes20202.poo.aula04.exemplo2;

import java.util.Scanner;

public class Banco {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		ContaCorrente contaCorrente1 = new ContaCorrente();

		contaCorrente1.numero = 10;
		contaCorrente1.nomeCorrentista = "Claudio Neiva";
		contaCorrente1.saldo = 500d;

		// interface para solicitar o tipo da operação e o valor
		System.out.println("Informe o valor do saque:");
		Double valorSaque = scanner.nextDouble();
		contaCorrente1.sacar(valorSaque);

		System.out.println("saldo=" + contaCorrente1.consultarSaldo());

	}

}
