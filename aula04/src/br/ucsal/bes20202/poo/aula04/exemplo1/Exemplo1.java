package br.ucsal.bes20202.poo.aula04.exemplo1;

public class Exemplo1 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		aluno1.matricula = 123;
		aluno1.nome = "Claudio Neiva";
		aluno1.email = "antonio.neiva@pro.ucsal.br";
		aluno1.anoIngresso = 2019;

		Aluno aluno2 = new Aluno();
		aluno2.matricula = 123;
		aluno2.nome = "Claudio Neiva";
		aluno2.email = "antonio.neiva@pro.ucsal.br";
		aluno2.anoIngresso = 2019;

		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);

		if (aluno1 == aluno2) {
			System.out.println("aluno1 APONTA PARA A MESMA INSTÂNCIA APONTADA POR aluno2.");
		} else {
			System.out.println("aluno1 APONTA PARA UMA INSTÂNCIA DIFERENTE DA APONTADA POR aluno2.");
		}

		if (aluno1.equals(aluno2)) {
			System.out.println("aluno1 igual ao aluno2.");
		} else {
			System.out.println("aluno1 diferente do aluno2.");
		}

	}

}
