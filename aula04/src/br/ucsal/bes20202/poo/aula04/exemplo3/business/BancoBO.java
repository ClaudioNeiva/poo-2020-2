package br.ucsal.bes20202.poo.aula04.exemplo3.business;

import br.ucsal.bes20202.poo.aula04.exemplo3.domain.ContaCorrente;
import br.ucsal.bes20202.poo.aula04.exemplo3.persistence.ContaCorrentDAO;

public class BancoBO {

	public static void registrarContaCorrente(ContaCorrente contaCorrente) throws Exception {
		// Validar a contaCorrente
		if (contaCorrente.nomeCorrentista.length() < 10) {
			// O nome do correntista não é valido!!!!
			throw new Exception("Nome do correntista inválido! O nome deve ter no mínimo 10 caracters.");
		}
		ContaCorrentDAO.salvar(contaCorrente);
	}

}
