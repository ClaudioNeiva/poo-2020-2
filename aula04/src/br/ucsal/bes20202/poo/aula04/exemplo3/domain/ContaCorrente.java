package br.ucsal.bes20202.poo.aula04.exemplo3.domain;

// FIXME Modificar a visibilidade dos atributos para private.
public class ContaCorrente {

	public Integer numero;

	public String nomeCorrentista;

	public Double saldo;

	public Double vconsultarSaldo() {
		return saldo;
	}

	public void sacar(Double valor) {
		saldo -= valor;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

}
