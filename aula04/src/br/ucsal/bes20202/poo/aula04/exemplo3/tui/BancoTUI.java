package br.ucsal.bes20202.poo.aula04.exemplo3.tui;

import java.util.Scanner;

import br.ucsal.bes20202.poo.aula04.exemplo3.business.BancoBO;
import br.ucsal.bes20202.poo.aula04.exemplo3.domain.ContaCorrente;

public class BancoTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		cadastrarContaCorrente();
	}

	private static void cadastrarContaCorrente() {
		Integer numeroConta;
		String nomeCorrentista;
		System.out.println("Informar o número da conta:");
		numeroConta = scanner.nextInt();
		System.out.println("Informar o nome do correntista:");
		nomeCorrentista = scanner.nextLine();

		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.numero = numeroConta;
		contaCorrente.nomeCorrentista = nomeCorrentista;

		try {
			BancoBO.registrarContaCorrente(contaCorrente);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
