package br.ucsal.bes20202.poo.aula04.exemplo3.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.poo.aula04.exemplo3.domain.ContaCorrente;

public class ContaCorrentDAO {

	// Abordagem com Array
	private static ContaCorrente[] contasCorrenteArray = new ContaCorrente[10];
	
	// Abordagem com List
	private static List<ContaCorrente> contasCorrente = new ArrayList<>();

	public static void salvar(ContaCorrente contaCorrente) {
		contasCorrente.add(contaCorrente);
	}

}
