package br.ucsal.bes20202.poo.atividade07.geradoporferramenta;

/**
 * Class Veiculo Veículos genéricos
 */
public abstract class Veiculo {

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	private Pessoa comprador;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

}
