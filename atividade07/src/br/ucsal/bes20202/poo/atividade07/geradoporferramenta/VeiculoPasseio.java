package br.ucsal.bes20202.poo.atividade07.geradoporferramenta;

public class VeiculoPasseio extends Veiculo {

	private Integer quantidadeMaxPassageiros;

	// Capacidade do porta-malas em litros.
	private Integer capacidadePortaMalas;

	public Integer getQuantidadeMaxPassageiros() {
		return quantidadeMaxPassageiros;
	}

	public void setQuantidadeMaxPassageiros(Integer quantidadeMaxPassageiros) {
		this.quantidadeMaxPassageiros = quantidadeMaxPassageiros;
	}

	public Integer getCapacidadePortaMalas() {
		return capacidadePortaMalas;
	}

	public void setCapacidadePortaMalas(Integer capacidadePortaMalas) {
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

}
