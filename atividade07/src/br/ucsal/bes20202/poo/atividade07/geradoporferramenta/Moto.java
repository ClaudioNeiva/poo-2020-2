package br.ucsal.bes20202.poo.atividade07.geradoporferramenta;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer quantidadeCilindradas;

	public Integer getQuantidadeCilindradas() {
		return quantidadeCilindradas;
	}

	public void setQuantidadeCilindradas(Integer quantidadeCilindradas) {
		this.quantidadeCilindradas = quantidadeCilindradas;
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

}
