package br.ucsal.bes20202.poo.atividade07.geradoporferramenta;

public class VeiculoCarga extends Veiculo {

	// Capacidade de carga em toneladas.
	private Integer capacidadeCarga;

	private Integer quantidadeEixos;

	// Capacidade de combustível em litros.
	private Integer capacidadeCombustivel;

	public Integer getCapacidadeCarga() {
		return capacidadeCarga;
	}

	public void setCapacidadeCarga(Integer capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}

	public Integer getQuantidadeEixos() {
		return quantidadeEixos;
	}

	public void setQuantidadeEixos(Integer quantidadeEixos) {
		this.quantidadeEixos = quantidadeEixos;
	}

	public Integer getCapacidadeCombustivel() {
		return capacidadeCombustivel;
	}

	public void setCapacidadeCombustivel(Integer capacidadeCombustivel) {
		this.capacidadeCombustivel = capacidadeCombustivel;
	}

}
