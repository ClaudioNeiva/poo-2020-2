package br.ucsal.bes20202.poo.atividade07.feitoamao;

public class Exemplo {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa("252525252552", "Claudio Neiva");

		Moto moto1 = new Moto("ABC12F3", 2010, 15000.23, pessoa1, CategoriaMotoEnum.ESTRADA, 50);
		VeiculoCarga veiculoCarga1 = new VeiculoCarga("OPI1I23", 2015, 234000.45, pessoa1, 15, 8, 350);

		gerarRelatorio(moto1, veiculoCarga1);
	}

	private static void gerarRelatorio(Veiculo... veiculos) {
		System.out.println("Gerar relatório:");
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo.descrever());
		}
	}

}
