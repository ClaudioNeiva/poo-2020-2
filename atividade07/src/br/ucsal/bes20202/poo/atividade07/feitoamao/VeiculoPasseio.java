package br.ucsal.bes20202.poo.atividade07.feitoamao;

public class VeiculoPasseio extends Veiculo {

	private Integer quantidadeMaxPassageiros;

	private Integer capacidadePortaMalas;

	public VeiculoPasseio(String placa, Integer anoFabricacao, Double valor, Pessoa comprador,
			Integer quantidadeMaxPassageiros, Integer capacidadePortaMalas) {
		super(placa, anoFabricacao, valor, comprador);
		this.quantidadeMaxPassageiros = quantidadeMaxPassageiros;
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

	public Integer getQuantidadeMaxPassageiros() {
		return quantidadeMaxPassageiros;
	}

	public void setQuantidadeMaxPassageiros(Integer quantidadeMaxPassageiros) {
		this.quantidadeMaxPassageiros = quantidadeMaxPassageiros;
	}

	public Integer getCapacidadePortaMalas() {
		return capacidadePortaMalas;
	}

	public void setCapacidadePortaMalas(Integer capacidadePortaMalas) {
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

}
