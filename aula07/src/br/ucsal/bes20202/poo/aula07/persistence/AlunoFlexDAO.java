package br.ucsal.bes20202.poo.aula07.persistence;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import br.ucsal.bes20202.poo.aula07.domain.Aluno;

public class AlunoFlexDAO {

	private List<Aluno> alunos;

	public AlunoFlexDAO(String listType) {
		if (listType.equals("ArrayList")) {
			alunos = new ArrayList<>();
		} else if (listType.equals("LinkedList")) {
			alunos = new LinkedList<>();
		}
		throw new RuntimeException("ListType inválido!");
	}

	/**
	 * Armazenar o aluno passado como parâmetro
	 * 
	 * @param aluno aluno que será armazenado
	 */
	public void salvar(Aluno aluno) {
		alunos.add(aluno);
	}

	/**
	 * Retornar a instância do aluno correspondente à matrícula passada como
	 * parâmetro.
	 * 
	 * @param matricula a matrícula do aluno desejado
	 * @return a intância do aluno
	 * @throws RuntimeException quando aluno não for encontrado
	 */
	public Aluno consultar(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		throw new RuntimeException("Aluno não encontrado.");
	}

	/**
	 * Retornar o aluno da posição indicada.
	 * 
	 * @param posicao do aluno a ser retornado.
	 * @return aluno da posição informada.
	 */
	public Aluno obterPorPosicao(Integer posicao) {
		return alunos.get(posicao);
	}

	/**
	 * Excluir o aluno da posição especificada.
	 * 
	 * @param posicao do aluno que será excluído.
	 */
	public void excluir(int posicao) {
		alunos.remove(posicao);
	}

	/**
	 * Retornar a quantidade de alunos armazenados.
	 * 
	 * @return quantidade de alunos armazenados.
	 */
	public int size() {
		return alunos.size();
	}

}
