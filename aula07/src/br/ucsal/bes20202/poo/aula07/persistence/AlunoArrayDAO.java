package br.ucsal.bes20202.poo.aula07.persistence;

import br.ucsal.bes20202.poo.aula07.domain.Aluno;

public class AlunoArrayDAO {

	private static final int QTD_MAX_ALUNOS = 100;

	private static Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];
	private static int qtd = 0;

	/**
	 * Armazenar o aluno passado como parâmetro
	 * 
	 * @param aluno aluno que será armazenado
	 */
	public static void salvar(Aluno aluno) {
		if (qtd == QTD_MAX_ALUNOS) {
			throw new RuntimeException("Array cheio, não é possível salvar nos alunos!");
		}
		alunos[qtd] = aluno;
		qtd++;
	}

	/**
	 * Retornar a instância do aluno correspondente à matrícula passada como
	 * parâmetro.
	 * 
	 * @param matricula a matrícula do aluno desejado
	 * @return a intância do aluno
	 * @throws RuntimeException quando aluno não for encontrado
	 */
	public static Aluno consultar(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		throw new RuntimeException("Aluno não encontrado.");
	}

	/**
	 * Retornar o aluno da posição indicada.
	 * 
	 * @param posicao do aluno a ser retornado.
	 * @return aluno da posição informada.
	 */
	public static Aluno obterPorPosicao(Integer posicao) {
		validarPosicao(posicao);
		return alunos[posicao];
	}

	/**
	 * Excluir o aluno da posição especificada.
	 * 
	 * @param posicao do aluno que será excluído.
	 */
	public static void excluir(Integer posicao) {
		validarPosicao(posicao);
		for (int i = posicao; i < qtd - 1; i++) {
			alunos[i] = alunos[i + 1];
		}
		qtd--;
		alunos[qtd] = null;
	}

	/**
	 * Retornar a quantidade de alunos armazenados.
	 * 
	 * @return quantidade de alunos armazenados.
	 */
	public static int size() {
		return qtd;
	}

	private static void validarPosicao(Integer posicao) {
		if (posicao < 0 || posicao > qtd) {
			throw new RuntimeException("Posição inválida!");
		}
	}

}
