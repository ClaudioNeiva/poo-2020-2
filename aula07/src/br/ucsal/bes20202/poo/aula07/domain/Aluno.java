package br.ucsal.bes20202.poo.aula07.domain;

public class Aluno {

	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	private Integer anoIngresso;

	// public Aluno(String ) {
	public Aluno(String nome) {
		this.nome = nome;
		contador++;
		this.matricula = contador;
	}

	// public Aluno(String , String ) {
	public Aluno(String nome, String email) {
		this(nome);
		this.email = email;
	}

	// public Aluno(String, Integer ) {
	public Aluno(String nome, Integer anoIngresso) {
		this(nome);
		this.anoIngresso = anoIngresso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAnoIngresso() {
		return anoIngresso;
	}

	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}

	public Integer getMatricula() {
		return matricula;
	}

}
