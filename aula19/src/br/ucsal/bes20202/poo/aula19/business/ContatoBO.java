package br.ucsal.bes20202.poo.aula19.business;

import java.util.List;

import br.ucsal.bes20202.poo.aula19.domain.Contato;
import br.ucsal.bes20202.poo.aula19.persistence.ContatoDAO;

public class ContatoBO {

	private ContatoBO() {
	}

	public static void incluir(Contato contato) {
		validar(contato);
		ContatoDAO.incluir(contato);
	}

	public static List<Contato> obterTodos() {
		return ContatoDAO.obterTodos();
	}

	private static void validar(Contato contato) {
		// TODO Implementar as validações do contato.
	}

}
