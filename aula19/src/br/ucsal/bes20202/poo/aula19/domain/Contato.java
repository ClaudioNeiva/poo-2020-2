package br.ucsal.bes20202.poo.aula19.domain;

import java.io.Serializable;
import java.util.List;

public class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;

	private String endereco;

	private List<String> telefones;

	public Contato(String nome, String endereco, List<String> telefones) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	@Override
	public String toString() {
		return "Contato [nome=" + nome + ", endereco=" + endereco + "]";
	}

}
