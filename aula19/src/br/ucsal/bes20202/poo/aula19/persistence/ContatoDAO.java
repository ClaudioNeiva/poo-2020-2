package br.ucsal.bes20202.poo.aula19.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import br.ucsal.bes20202.poo.aula19.domain.Contato;

public class ContatoDAO {

	private static final String NOME_ARQUIVO = "contato.dat";

	private static List<Contato> contatos;

	static {
		carregar();
	}

	private ContatoDAO() {
	}

	public static void incluir(Contato contato) {
		contatos.add(contato);
		salvar();
	}

	public static List<Contato> obterTodos() {
		List<Contato> contatosClone = new ArrayList<>(contatos);
		contatosClone.sort(Comparator.comparing(Contato::getNome));
		return contatosClone;
	}

	// FIXME Tratar melhor as exceções.
	private static void salvar() {
		File file = new File(NOME_ARQUIVO);
		// try-resource: fecha automaticamente instâncias que implementam AutoCloseable.
		try (FileOutputStream fileOutputStream = new FileOutputStream(file);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
			objectOutputStream.writeObject(contatos);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// FIXME Exceções não tratadas (que demandam dar mensagem pra o usuário deveriam
	// subir para o TUI).
	@SuppressWarnings("unchecked")
	private static void carregar() {
		File file = new File(NOME_ARQUIVO);
		try (FileInputStream fileInputStream = new FileInputStream(file);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);) {
			contatos = (List<Contato>) objectInputStream.readObject();
		} catch (FileNotFoundException e) {
			contatos = new ArrayList<>();
		} catch (IOException | ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}

}
