package br.ucsal.bes20202.poo.aula19.tui;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.poo.aula19.business.ContatoBO;
import br.ucsal.bes20202.poo.aula19.domain.Contato;

public class ContatoTUI {

	private ContatoTUI() {
	}

	// TODO Implementar a inclusão de telefones.
	public static void incluir() {
		String nome;
		String endereco;
		List<String> telefones = new ArrayList<>();
		System.out.println("############### INCLUSÃO DE CONTATOS ###############");
		System.out.println("Informe os dados do contato:");
		System.out.print("Nome:");
		nome = ScannerUtil.scanner.nextLine();
		System.out.print("Endereço:");
		endereco = ScannerUtil.scanner.nextLine();
		Contato contato = new Contato(nome, endereco, telefones);
		ContatoBO.incluir(contato);
		System.out.println("Contato incluído com sucesso.");
	}

	public static void consultar() {
		System.out.println("############### CONSULTA DE CONTATOS ###############");
		List<Contato> contatos = ContatoBO.obterTodos();
		System.out.println("Contatos:");
		for (Contato contato : contatos) {
			System.out.println("\nNome:" + contato.getNome());
			System.out.println("Endereço:" + contato.getEndereco());
			System.out.println("Telefone:" + contato.getTelefones());
		}
	}

}
