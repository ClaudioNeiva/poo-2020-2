package br.ucsal.bes20202.poo.aula19.tui;

public class MenuTUI {
	
	private MenuTUI() {
	}
	
	public static void executar() {
		AgendaMenuEnum opcao;
		do {
			exibir();
			opcao = obterOpcao();
			executarOpcao(opcao);
		} while (!opcao.equals(AgendaMenuEnum.SAIR));
	}

	private static void exibir() {
		System.out.println("\n\n\n");
		for (AgendaMenuEnum agendaMenu : AgendaMenuEnum.values()) {
			System.out.println(agendaMenu.obterDescricaoDetalhada());
		}
	}

	// FIXME Tratar a escolha de opções não válidas. 
	private static AgendaMenuEnum obterOpcao() {
		Integer opcaoInteger;
		System.out.print("Informe a opção desejada:");
		opcaoInteger = ScannerUtil.scanner.nextInt();
		ScannerUtil.scanner.nextLine();
		return AgendaMenuEnum.valueOfCodigo(opcaoInteger);
	}

	private static void executarOpcao(AgendaMenuEnum opcao) {
		System.out.println("\n\n\n");
		switch (opcao) {
		case INCLUIR:
			ContatoTUI.incluir();
			break;
		case LISTAR:
			ContatoTUI.consultar();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		}
	}

}
