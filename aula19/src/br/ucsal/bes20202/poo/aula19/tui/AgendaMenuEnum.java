package br.ucsal.bes20202.poo.aula19.tui;

public enum AgendaMenuEnum {

	INCLUIR(1, "Incluir"),

	LISTAR(2, "Listar"),

	SAIR(9, "Sair");

	private Integer codigo;
	
	private String descricao;

	private AgendaMenuEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public String obterDescricaoDetalhada() {
		return codigo + " - " + descricao;
	}

	public static AgendaMenuEnum valueOfCodigo(Integer codigo) {
		for (AgendaMenuEnum agendaMenu : values()) {
			if (agendaMenu.codigo.equals(codigo)) {
				return agendaMenu;
			}
		}
		throw new IllegalArgumentException("AgendaMenuEnum.codigo(" + codigo + ") não existe");
	}

}
