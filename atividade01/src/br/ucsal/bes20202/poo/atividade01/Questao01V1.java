package br.ucsal.bes20202.poo.atividade01;

import java.util.Scanner;

public class Questao01V1 {

	public static void main(String[] args) {
		obterNotaApresentarConceito();
	}
	
	public static void obterNotaApresentarConceito() {
		Scanner scanner = new Scanner(System.in);
		Integer nota;
		String conceito;

		// Entrada de dados
		System.out.println("Informe uma nota (0 a 100 - intervalo fechado):");
		nota = scanner.nextInt();

		// Processamento
		// Obs: Não é necessário tratar valores fora da faixa.
		if (nota <= 49) {
			conceito = "insuficiente";
		} else if (nota <= 64) {
			conceito = "regular";
		} else if (nota <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}

		// Saída
		System.out.println("Conceito = " + conceito);
	}

}
