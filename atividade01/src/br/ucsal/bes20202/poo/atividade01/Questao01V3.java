package br.ucsal.bes20202.poo.atividade01;

import java.util.Scanner;

public class Questao01V3 {

	public static void main(String[] args) {
		obterNotaApresentarConceito();
	}

	public static void obterNotaApresentarConceito() {
		Integer nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	public static Integer obterNota() {
		Integer nota;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe uma nota (0 a 100 - intervalo fechado):");
		nota = scanner.nextInt();
		return nota;
	}

	public static String calcularConceito(Integer nota) {
		String conceito;
		// Obs: Não é necessário tratar valores fora da faixa.
		if (nota <= 49) {
			conceito = "insuficiente";
		} else if (nota <= 64) {
			conceito = "regular";
		} else if (nota <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}
		return conceito;
	}

	public static void exibirConceito(String conceito) {
		System.out.println("Conceito = " + conceito);
	}

}
