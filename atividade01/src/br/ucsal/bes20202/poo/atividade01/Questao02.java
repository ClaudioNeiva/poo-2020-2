package br.ucsal.bes20202.poo.atividade01;

import java.util.Scanner;

/*

Crie um programa em Java que lê um valor N, inteiro e positivo, 
calcula e escreve o valor de E (soma dos inversos dos fatoriais de 0 a N):

E = 1      + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!
E = 1 / 0! + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!

Exemplo 1
Se N = 5, então:
E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + 1 / 4! + 1 / 5!

Exemplo 2
Se N = 0, então:
E = 1

Exemplo 3
Se N = 1, então:
E = 1 + 1 / 1!

CASOS DE TESTE
Dado de entrada -> Saída esperada
N=0 -> E=1
N=2 -> E=2.5
N=3 -> E=2.667 (aproximação)

*/

public class Questao02 {

	public static void main(String[] args) {
		obterNCalcularExibirE();
	}

	public static void obterNCalcularExibirE() {
		Integer n;
		Double e;

		n = obterNumeroInteiroPositivo();
		e = calcularE(n);
		exibirE(n, e);
	}

	// FIXME Tratar entrada de dados literais (necessário saber tratar exceptions).
	public static Integer obterNumeroInteiroPositivo() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		do {
			System.out.println("Informe um número maior ou igual a zero:");
			n = scanner.nextInt();
			if (n < 0) {
				System.out.println("Número fora da faixa.");
			}
		} while (n < 0);
		return n;
	}

	// Exemplo
	// entrada: n=5
	// e = 0 + 1 + 2 + 3 + 4 + 5 (v1)
	// e = 0! + 1! + 2! + 3! + 4! + 5! (v2)
	// e = 1/0! + 1/1! + 1/2! + 1/3! + 1/4! + 1/5! (v3 - final)
	public static Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			Long fatorial = calcularFatorial(i);
			e = e + 1d / fatorial;
		}
		return e;
	}

	public static Long calcularFatorial(int x) {
		Long fatorial = 1L;
		for (int i = 1; i <= x; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	// Curiosidade1
	public static Long calcularFatorialRecursivo(int x) {
		if (x == 0) {
			return 1L;
		}
		return x * calcularFatorialRecursivo(x - 1);
	}

	// Curiosidade2
	public static Long calcularFatorialRecursivoOperadorTernario(int x) {
		return x == 0 ? 1L : x * calcularFatorialRecursivo(x - 1);
	}

	// Exemplo:
	// entrada: n=2 -> e=2.5
	// saída esperada: E(2)=2.5
	public static void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

}
