package br.ucsal.bes20202.poo.atividade01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Questao01Test {

	@Test
	public void testarConceitoInsuficiente() {
		// Dados de entrada
		Integer nota = 30;

		// Saída esperada (resultado esperado)
		String conceitoEsperado = "insuficiente";

		// Executo o método que eu quero testar e armazeno o resultado atual
		String conceitoAtual = Questao01.calcularConceito(nota);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertEquals(conceitoEsperado, conceitoAtual);
	}

	@Test
	public void testarConceitoRegular() {
		// Dados de entrada
		Integer nota = 55;

		// Saída esperada (resultado esperado)
		String conceitoEsperado = "regular";

		// Executo o método que eu quero testar e armazeno o resultado atual
		String conceitoAtual = Questao01.calcularConceito(nota);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertEquals(conceitoEsperado, conceitoAtual);
	}

	@ParameterizedTest
	@CsvSource({ "30, insuficiente", "53, regular", "70, bom", "90, ótimo" })
	public void testarConceito(Integer nota, String conceitoEsperado) {
		// Executo o método que eu quero testar e armazeno o resultado atual
		String conceitoAtual = Questao01.calcularConceito(nota);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertEquals(conceitoEsperado, conceitoAtual);
	}

}
