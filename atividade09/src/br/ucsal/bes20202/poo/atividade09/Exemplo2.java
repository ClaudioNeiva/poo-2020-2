package br.ucsal.bes20202.poo.atividade09;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class Exemplo2 {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		String dataNascimentoString;
		LocalDate dataNascimento;

		System.out.println("Informe sua data de nascimento:");
		dataNascimentoString = scanner.nextLine();
		Locale locale = new Locale("pt");
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy", locale);
		DateTimeFormatter dateFormat2 = DateTimeFormatter.ofPattern("MMMM", locale);
		dataNascimento = LocalDate.parse(dataNascimentoString, dateFormat);

		System.out.println("Data de nascimento=" + dataNascimento);
		System.out.println("Dia do dataNascimento=" + dataNascimento.getMonthValue());
		System.out.println("Dia da semana do dataNascimento=" + dataNascimento.getDayOfWeek());
		System.out.println("Dia da semana do dataNascimento=" + dataNascimento.format(dateFormat2));
		System.out.println("Dia da semana de hoje=" + LocalDate.now().getDayOfWeek());
	}

}
