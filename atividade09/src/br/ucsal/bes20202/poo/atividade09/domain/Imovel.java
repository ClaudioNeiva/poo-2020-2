package br.ucsal.bes20202.poo.atividade09.domain;

import java.time.LocalDate;

import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;

public abstract class Imovel {

	private Integer codigo;

	private LocalDate dataCadastro;
	
	private String endereco;

	private String bairro;

	// java.math.BigDecimal
	private Double valor;
	
	// Exemplo de atributo protected
	protected Double valorVenal;

	public Imovel(Integer codigo, String endereco, String bairro, Double valor) throws ValorImovelInvalidoException {
		super();
		setValor(valor);
		this.codigo = codigo;
		this.endereco = endereco;
		this.bairro = bairro;
	}

	public abstract Double obterValorImposto();
	
	public void setValorVenal(Double valorVenal) {
		this.valorVenal = valorVenal;;
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws ValorImovelInvalidoException {
		if (valor <= 0) {
			throw new ValorImovelInvalidoException();
		}
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Imovel [codigo=" + codigo + ", endereco=" + endereco + ", bairro=" + bairro + ", valor=" + valor + "]";
	}

}

//	public void setValor(Double valor) throws ValorInvalidoException {
//		if (valor <= 0) {
//			throw new ValorInvalidoException("Valor do imóvel inválido. O valor do imóvel deve ser maior que zero!");
//		}
//		this.valor = valor;
//	}
