package br.ucsal.bes20202.poo.atividade09.domain;

import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;

public class Apartamento extends Imovel {

	// java.math.BigDecimal
	private Double areaFracaoIdeal;

	// java.math.BigDecimal
	private Double areaPrivativa;

	public Apartamento(Integer codigo, String endereco, String bairro, Double valor, Double areaFracaoIdeal,
			Double areaPrivativa) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaFracaoIdeal = areaFracaoIdeal;
		this.areaPrivativa = areaPrivativa;
	}
	
	@Override
	public Double obterValorImposto() {
		return 100 * areaPrivativa + 30 * areaFracaoIdeal;
	}

	public Double getAreaFracaoIdeal() {
		return areaFracaoIdeal;
	}

	public void setAreaFracaoIdeal(Double areaFracaoIdeal) { // throws ValorInvalidoException {
//		if (areaFracaoIdeal <= 0) {
//			throw new ValorInvalidoException(MensagemEnum.FRACAO_IDEAL_INVALIDA.obterDescricao());
//		}
		this.areaFracaoIdeal = areaFracaoIdeal;
	}

	public Double getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(Double areaPrivativa) {// throws ValorInvalidoException {
//		if(areaFracaoIdeal<=0) {
//			throw new ValorInvalidoException(MensagemEnum.AREA_PRIVATVA_INVALIDA.obterDescricao());
//		}
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public String toString() {
		return "Apartamento [areaFracaoIdeal=" + areaFracaoIdeal + ", areaPrivativa=" + areaPrivativa + ", toString()="
				+ super.toString() + "]";
	}

}
