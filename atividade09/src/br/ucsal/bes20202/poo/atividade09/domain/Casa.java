package br.ucsal.bes20202.poo.atividade09.domain;

import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;

public class Casa extends Imovel {

	// java.math.BigDecimal
	private Double areaTerreno;

	// java.math.BigDecimal
	private Double areaConstruida;

	public Casa(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno, Double areaConstruida)
			throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaTerreno = areaTerreno;
		this.areaConstruida = areaConstruida;
	}

	@Override
	public Double obterValorImposto() {
		return 0.1 * valorVenal + 120 * areaConstruida + 15 * areaTerreno;
	}

	public Double getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(Double areaTerreno) {
		this.areaTerreno = areaTerreno;
	}

	public Double getAreaConstruida() {
		return areaConstruida;
	}

	public void setAreaConstruida(Double areaConstruida) {
		this.areaConstruida = areaConstruida;
	}

	@Override
	public String toString() {
		return "Casa [areaTerreno=" + areaTerreno + ", areaConstruida=" + areaConstruida + ", toString()="
				+ super.toString() + "]";
	}

}
