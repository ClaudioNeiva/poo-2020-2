package br.ucsal.bes20202.poo.atividade09.exception;

public class ValorInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValorInvalidoException(String message) {
		super(message);
	}

}
