package br.ucsal.bes20202.poo.atividade09;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.poo.atividade09.domain.Apartamento;
import br.ucsal.bes20202.poo.atividade09.domain.Casa;
import br.ucsal.bes20202.poo.atividade09.domain.Imovel;

public class Exemplo {

	public static void main(String[] args) {
		List<Imovel> imoveis = new ArrayList<>();

		// imoveis.add(new Apartamento(12, "rua x"...
		// imoveis.add(new Casa(5665, "rua y"...

		Double valorTotalImoveis = calcularValorTotalImoveis(imoveis);
		System.out.println("O valor total de todos os imóveis é " + valorTotalImoveis);

		Double valorTotalImposto = calcularImpostoTotal(imoveis);
		System.out.println("O valor total do imposto de todos os imóveis é " + valorTotalImposto);
	}

	private static Double calcularValorTotalImoveis(List<Imovel> imoveis) {
		Double valorTotalImovel = 0d;
		for (Imovel imovel : imoveis) {
			valorTotalImovel += imovel.getValor();
		}
		return valorTotalImovel;
	}

	private static Double calcularImpostoTotal(List<Imovel> imoveis) {
		Double valorTotalImposto = 0d;
		for (Imovel imovel : imoveis) {
			valorTotalImposto += imovel.obterValorImposto();
		}
		return valorTotalImposto;
	}

	// Não devemos fazer assim!
	private static Double calcularImpostoTotalNAO(List<Imovel> imoveis) {
		Double valorTotal = 0d;
		for (Imovel imovel : imoveis) {
			if (imovel instanceof Apartamento) {
				valorTotal += ((Apartamento) imovel).obterValorImposto();
			} else if (imovel instanceof Casa) {
				valorTotal += ((Casa) imovel).obterValorImposto();
			}
		}
		return valorTotal;
	}

}
