package br.ucsal.bes20202.poo.atividade09.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.poo.atividade09.domain.Imovel;

public class ImovelDAO {

	// Essa lista representa o banco de dados da aplicação!
	private static List<Imovel> imoveis = new ArrayList<>();

	private ImovelDAO() {
	}
	
	public static void salvar(Imovel imovel) {
		imoveis.add(imovel);
	}

	public static List<Imovel> obterTodos() {
		return new ArrayList<>(imoveis);
	}

}
