package br.ucsal.bes20202.poo.atividade09.businesspersistenceapresentacao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes20202.poo.atividade09.domain.Casa;
import br.ucsal.bes20202.poo.atividade09.domain.Imovel;
import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;

public class Imobiliaria {

	/*
	 * Atributo estático: uma lista para todos os imóveis, tanto casas quanto
	 * apartamentos. A lista deve ser instanciada na declaração do atributo;
	 * 
	 */
	private static List<Imovel> imoveis = new ArrayList<>();

	/*
	 * cadastrarCasa: este método deve receber os atributos necessários à
	 * instanciação de um objeto da classe Casa e adicioná-lo à lista de imóveis.
	 * Lembre-se de realizar o tratamento de exceção;
	 * 
	 */
	public static void cadastrarCasa(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno,
			Double areaConstruida) {
		try {
			Casa casa = new Casa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
			imoveis.add(casa);
		} catch (ValorImovelInvalidoException e) {
			System.out.println("O valor do imóvel não é válido!");
		}
	}

	/*
	 * listarImoveisPorCodigo: este método deve listar os imóveis por ordem
	 * crescente de código. Esta deve ser a ordenação padrão para imóveis (a
	 * implementação do critério de ordenação deve ser feita na classe de imóvel).
	 * Sua listagem deve conter: código, endereço e bairro;
	 */
	public static void listarImoveisPorCodigo() {
		imoveis.sort(Comparator.comparing(Imovel::getCodigo));
		for (Imovel imovel : imoveis) {
			System.out.println("Código=" + imovel.getCodigo());
			System.out.println("Endereço=" + imovel.getEndereco());
			System.out.println("Bairro=" + imovel.getBairro());
			System.out.println("------------------------------");
		}
	}

	/*
	 * EXTRA: ilustrando o uso do DownCast! listarImoveisPorCodigo: este método deve
	 * listar os imóveis por ordem crescente de código. Esta deve ser a ordenação
	 * padrão para imóveis (a implementação do critério de ordenação deve ser feita
	 * na classe de imóvel). Sua listagem deve conter: código, endereço, bairro e,
	 * para casas, área do terreno;
	 */
	public static void listarImoveisPorCodigoExtra() {
		imoveis.sort(Comparator.comparing(Imovel::getCodigo));
		for (Imovel imovel : imoveis) {
			System.out.println("Código=" + imovel.getCodigo());
			System.out.println("Endereço=" + imovel.getEndereco());
			System.out.println("Bairro=" + imovel.getBairro());
			if (imovel instanceof Casa) {
				Casa casa = (Casa) imovel;
				System.out.println("Casa com área de terreno=" + casa.getAreaTerreno());
				// System.out.println("Casa com área de terreno=" + ((Casa)
				// imovel).getAreaTerreno());
			}
			System.out.println("------------------------------");
		}
	}

	/*
	 * listarImoveisPorBairroValor: este método deve listar os imóveis, por ordem
	 * crescente de Bairro e por ordem crescente de valor dentro de cada Bairro. Sua
	 * listagem deve conter: código, endereço e valor do imposto;
	 */
	public static void listarImoveisPorBairroValor() {
		imoveis.sort(Comparator.comparing(Imovel::getBairro).thenComparing(Imovel::getValor));
		for (Imovel imovel : imoveis) {
			System.out.println("Código=" + imovel.getCodigo());
			System.out.println("Endereço=" + imovel.getEndereco());
			System.out.println("Valor do imposto=" + imovel.obterValorImposto());
			System.out.println("------------------------------");
		}
	}

	/*
	 * listarBairros: este método deve listar os bairros onde existem imóveis
	 * cadastrados. Não devem ser exibidos nomes de bairro repetidos;
	 */
	public static void listarBairros() {
		Set<String> bairros = new HashSet<>();
		for (Imovel imovel : imoveis) {
			bairros.add(imovel.getBairro());
		}
		System.out.println("Os bairros onde existem imóveis cadastrados são:");
		for (String bairro : bairros) {
			System.out.println(bairro);
		}
		// bairros.forEach(System.out::println);
	}

}
