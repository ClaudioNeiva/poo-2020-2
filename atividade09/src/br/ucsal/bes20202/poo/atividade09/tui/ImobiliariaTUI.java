package br.ucsal.bes20202.poo.atividade09.tui;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import br.ucsal.bes20202.poo.atividade09.business.ImobiliariaBO;
import br.ucsal.bes20202.poo.atividade09.domain.Imovel;
import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;

public class ImobiliariaTUI {

	private static final Scanner scanner = new Scanner(System.in);

	private ImobiliariaTUI() {
	}
	
	public static void cadastrarCasa() {
		Integer codigo;
		String endereco;
		String bairro;
		Double valor;
		Double areaTerreno;
		Double areaConstruida;
		System.out.println("Informe os dados da casa:");
		System.out.println("Código:");
		codigo = scanner.nextInt();
		System.out.println("Endereço:");
		endereco = scanner.nextLine();
		System.out.println("Bairro:");
		bairro = scanner.nextLine();
		System.out.println("Valor:");
		valor = scanner.nextDouble();
		System.out.println("Área do terreno:");
		areaTerreno = scanner.nextDouble();
		System.out.println("Área construída:");
		areaConstruida = scanner.nextDouble();
		
		try {
			ImobiliariaBO.cadastrarCasa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
		} catch (ValorImovelInvalidoException e) {
			System.out.println("O valor do imóvel não é válido!");
		}
	}

	public static void listarImoveisPorCodigo() {
		List<Imovel> imoveis = ImobiliariaBO.obterImoveisPorOrdemCodigo();
		for (Imovel imovel : imoveis) {
			System.out.println("Código=" + imovel.getCodigo());
			System.out.println("Endereço=" + imovel.getEndereco());
			System.out.println("Bairro=" + imovel.getBairro());
			System.out.println("------------------------------");
		}
	}
	
	public static void listarImoveisPorBairroValor() {
		List<Imovel> imoveis = ImobiliariaBO.obterImoveisPorOrdemBairroValor();
		for (Imovel imovel : imoveis) {
			System.out.println("Código=" + imovel.getCodigo());
			System.out.println("Endereço=" + imovel.getEndereco());
			System.out.println("Valor do imposto=" + imovel.obterValorImposto());
			System.out.println("------------------------------");
		}
	}

	public static void listarBairros() {
		Set<String> bairros = ImobiliariaBO.obterBairrosComImoveisCadastrados();
		System.out.println("Os bairros onde existem imóveis cadastrados são:");
		for (String bairro : bairros) {
			System.out.println(bairro);
		}
	}

}
