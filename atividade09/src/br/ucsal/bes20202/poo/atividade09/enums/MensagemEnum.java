package br.ucsal.bes20202.poo.atividade09.enums;

public enum MensagemEnum {

	FRACAO_IDEAL_INVALIDA("A fração ideal deve ser maior que zero!", 23L),

	AREA_PRIVATVA_INVALIDA("A área privativa deve ser maior que zero!", 56L);

	private final String mensagem;

	private final Long codigo;

	private MensagemEnum(String mensagem, Long codigo) {
		this.mensagem = mensagem;
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public Long getCodigo() {
		return codigo;
	}

	public String obterDescricao() {
		return codigo + " - " + mensagem;
	}

}
