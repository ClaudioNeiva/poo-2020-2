package br.ucsal.bes20202.poo.atividade09.business;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes20202.poo.atividade09.domain.Casa;
import br.ucsal.bes20202.poo.atividade09.domain.Imovel;
import br.ucsal.bes20202.poo.atividade09.exception.ValorImovelInvalidoException;
import br.ucsal.bes20202.poo.atividade09.persistence.ImovelDAO;

public class ImobiliariaBO {

	private ImobiliariaBO() {
	}
	
	public static void cadastrarCasa(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno,
			Double areaConstruida) throws ValorImovelInvalidoException {
		Casa casa = new Casa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
		ImovelDAO.salvar(casa);
	}

	public static List<Imovel> obterImoveisPorOrdemCodigo() {
		List<Imovel> imoveis = ImovelDAO.obterTodos();
		imoveis.sort(Comparator.comparing(Imovel::getCodigo));
		return imoveis;
	}

	public static List<Imovel> obterImoveisPorOrdemBairroValor() {
		List<Imovel> imoveis = ImovelDAO.obterTodos();
		imoveis.sort(Comparator.comparing(Imovel::getBairro).thenComparing(Imovel::getValor));
		return imoveis;
	}

	public static Set<String> obterBairrosComImoveisCadastrados() {
		List<Imovel> imoveis = ImovelDAO.obterTodos();
		Set<String> bairros = new HashSet<>();
		for (Imovel imovel : imoveis) {
			bairros.add(imovel.getBairro());
		}
		return bairros;
	}

}
