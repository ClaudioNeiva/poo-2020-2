package br.ucsal.bes20202.poo.aula15.ordenacao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoJava8OuPosterior {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("Claudio", "Neiva", 1973, "caju@ucsal.br"));
		pessoas.add(new Pessoa("Felipe", "Diniz", 2000, "manga@ucsal.br"));
		pessoas.add(new Pessoa("Guilherme", "Bacelar", 2001, "melancia@ucsal.br"));
		pessoas.add(new Pessoa("Henrique", "Diniz", 2000, "goiaba@ucsal.br"));
		pessoas.add(new Pessoa("Mateus", "Neiva", 2002, "jaca@ucsal.br"));
		pessoas.add(new Pessoa("Carlos", "Bacelar", 2000, "pera@ucsal.br"));
		pessoas.add(new Pessoa("Eduardo", "Diniz", 2001, "uva@ucsal.br"));
		pessoas.add(new Pessoa("Manuela", "Neiva", 1999, "mamao@ucsal.br"));

		System.out.println("Pessoas na ordem que foram informadas:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		pessoas.sort(Comparator.naturalOrder());
		System.out.println("\nPessoas em ordem de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		pessoas.sort(Comparator.comparing(Pessoa::getNome));

		System.out.println("\nPessoas em ordem de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento));

		System.out.println("\nPessoas em ordem de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		pessoas.sort(Comparator.comparing(Pessoa::getSobrenome).thenComparing(Pessoa::getNome));

		System.out.println("\nPessoas em ordem de sobrenome e nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento).reversed());

		System.out.println("\nPessoas em ordem decrescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
