package br.ucsal.bes20202.poo.aula15.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Usando versões anteriores ao Java 1.8:
 * 
 * @author claudioneiva
 *
 */
public class OrdenacaoJavaAnteiorAo8 {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("Claudio", "Neiva", 1973, "caju@ucsal.br"));
		pessoas.add(new Pessoa("Felipe", "Diniz", 2000, "manga@ucsal.br"));
		pessoas.add(new Pessoa("Guilherme", "Bacelar", 2001, "melancia@ucsal.br"));
		pessoas.add(new Pessoa("Henrique", "Diniz", 2000, "goiaba@ucsal.br"));
		pessoas.add(new Pessoa("Mateus", "Neiva", 2002, "jaca@ucsal.br"));
		pessoas.add(new Pessoa("Carlos", "Bacelar", 2000, "pera@ucsal.br"));
		pessoas.add(new Pessoa("Eduardo", "Diniz", 2001, "uva@ucsal.br"));
		pessoas.add(new Pessoa("Manuela", "Neiva", 1999, "mamao@ucsal.br"));

		System.out.println("Pessoas na ordem que foram informadas:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// pessoas.sort(Comparator.naturalOrder());
		Collections.sort(pessoas);

		System.out.println("\nPessoas em ordem de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// pessoas.sort(Comparator.comparing(Pessoa::getNome));
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				return pessoa1.getNome().compareTo(pessoa2.getNome());
			}
		});

		System.out.println("\nPessoas em ordem de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento));
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				return pessoa1.getAnoNascimento().compareTo(pessoa2.getAnoNascimento());
			}
		});

		System.out.println("\nPessoas em ordem de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// pessoas.sort(Comparator.comparing(Pessoa::getSobrenome).thenComparing(Pessoa::getNome));
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				int resultado = pessoa1.getSobrenome().compareTo(pessoa2.getSobrenome());
				if (resultado == 0) {
					resultado = pessoa1.getNome().compareTo(pessoa2.getNome());
				}
				return resultado;
			}
		});

		System.out.println("\nPessoas em ordem de sobrenome e nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		//pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento).reversed());
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				return pessoa1.getAnoNascimento().compareTo(pessoa2.getAnoNascimento());
			}
		});
		Collections.reverse(pessoas);

		System.out.println("\nPessoas em ordem decrescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
