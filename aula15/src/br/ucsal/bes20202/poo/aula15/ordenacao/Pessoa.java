package br.ucsal.bes20202.poo.aula15.ordenacao;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private String sobrenome;

	private Integer anoNascimento;

	private String email;

	public Pessoa(String nome, String sobrenome, Integer anoNascimento, String email) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.anoNascimento = anoNascimento;
		this.email = email;
	}

	@Override
	public int compareTo(Pessoa outraPessoa) {
		return nome.compareTo(outraPessoa.nome);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", sobrenome=" + sobrenome + ", anoNascimento=" + anoNascimento + ", email="
				+ email + "]";
	}

}
