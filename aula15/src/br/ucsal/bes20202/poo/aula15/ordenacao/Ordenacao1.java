package br.ucsal.bes20202.poo.aula15.ordenacao;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ordenacao1 {

	public static void main(String[] args) {

		List<String> nomes = Arrays.asList("claudio", "maria", "pedro", "ana", "joão", "antonio");

		System.out.println("Nomes na ordem informada:" + nomes);

		nomes.sort(Comparator.naturalOrder());
		System.out.println("Nomes em ordem crescente:" + nomes);

		Collections.sort(nomes);
		System.out.println("Nomes em ordem crescente:" + nomes);

		// -1 menor < 0
		// 0 igual = 0
		// +1 maior > 0
		System.out.println("ana comparado com claudio=" + ("ana".compareTo("claudio")));
		System.out.println("pedro comparado com claudio=" + ("pedro".compareTo("claudio")));
		System.out.println("joao comparado com joao=" + ("joao".compareTo("joao")));
	}

}
