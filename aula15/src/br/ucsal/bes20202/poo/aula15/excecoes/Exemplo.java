package br.ucsal.bes20202.poo.aula15.excecoes;

import java.util.Scanner;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	// Ver aula10 que também exemplifica o polimorfismo

	public static void main(String[] args) {
		try {
			ContaCorrente contaCorrente1 = new ContaCorrente(56, "Claudio Neiva");
			ContaCorrente contaCorrente2 = new ContaCorrente(78, "Maria da Silva");
			ContaEspecial contaEspecial1 = new ContaEspecial(78, "Joaquim dos Santos", 700d);

			contaCorrente1.depositar(100d);
			contaCorrente2.depositar(500d);
			contaCorrente2.bloquear();
			contaEspecial1.depositar(900d);

			solicitarSaque(contaCorrente1);
			solicitarSaque(contaCorrente2);
			solicitarSaque(contaEspecial1);

			System.out.println("contaCorrente1=" + contaCorrente1);
			System.out.println("contaCorrente2=" + contaCorrente2);
			System.out.println("contaEspecial1=" + contaEspecial1);
		} catch (Throwable e) {
			System.out.println("O programa deu problema: " + e.getMessage());
		}
	}

	private static void solicitarSaque(ContaCorrente contaCorrente) {
		Double valorSaque;

		if (contaCorrente instanceof ContaEspecial) {
			ContaEspecial contaEspecial = (ContaEspecial) contaCorrente;
			System.out.println("Seu limite de crédito é " + contaEspecial.getLimiteCredito());
			// System.out.println("Seu limite de crédito é " + ((ContaEspecial)
			// contaCorrente).getLimiteCredito());
		}

		while (true) {
			System.out.println("Informe o valor do saque:");
			valorSaque = scanner.nextDouble();
			try {
				contaCorrente.sacar(valorSaque);
				System.out.println("Saque realizado com sucesso!");
				return;
			} catch (SaldoInsuficienteException e) {
				System.out.println(e.getMessage());
			} catch (ContaCorrenteBloqueadaException e) {
				System.out.println("Sua conta foi bloqueada!");
				return;
			}
		}

	}

}
