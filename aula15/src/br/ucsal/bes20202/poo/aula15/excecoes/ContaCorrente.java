package br.ucsal.bes20202.poo.aula15.excecoes;

public class ContaCorrente {

	private Integer numero;

	private String nomeCorrentista;

	protected Double saldo = 0d;

	protected SituacaoContaCorrente situacao = SituacaoContaCorrente.ATIVA;

	public ContaCorrente(Integer numero, String nomeCorrentista) {
		super();
		this.numero = numero;
		this.nomeCorrentista = nomeCorrentista;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws SaldoInsuficienteException, ContaCorrenteBloqueadaException {
		if (situacao.equals(SituacaoContaCorrente.BLOQUEADA)) {
			throw new ContaCorrenteBloqueadaException();
		}
		if (valor > saldo) {
			throw new SaldoInsuficienteException("Saldo insuficiente!");
		}
		saldo -= valor;
	}

	public void bloquear() {
		situacao = SituacaoContaCorrente.BLOQUEADA;
	}
	
	public void liberar() {
		situacao = SituacaoContaCorrente.ATIVA;
	}
	
	public SituacaoContaCorrente getSituacao() {
		return situacao;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public Double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaCorrente [numero=" + numero + ", nomeCorrentista=" + nomeCorrentista + ", saldo=" + saldo + "]";
	}

}
