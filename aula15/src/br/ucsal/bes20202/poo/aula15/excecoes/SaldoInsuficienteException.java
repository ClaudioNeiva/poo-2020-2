package br.ucsal.bes20202.poo.aula15.excecoes;

public class SaldoInsuficienteException extends Exception {

	public SaldoInsuficienteException(String message) {
		super(message);
	}

}
