package br.ucsal.bes20202.poo.aula06;

public class OffTopic1 {

	public static Integer somar(Integer... numeros) {
		// numeros é um array.
		Integer soma = 0;

		// for (int i = 0; i < numeros.length; i++) {
		// Integer n = numeros[i];
		// soma += n;
		// }

		for (Integer n : numeros) {
			soma += n;
		}
		return soma;
	}

	public static String concatenar(String nome, String... sobrenomes) {
		String nomeCompleto = nome;
		for (String sobrenome : sobrenomes) {
			nomeCompleto += " " + sobrenome;
		}
		return nomeCompleto;
	}

	public static void main(String[] args) {
		System.out.println("soma1=" + somar(10, 40, 50));
		System.out.println("soma2=" + somar(10, 40, 50, 12, 45));
		System.out.println("nome completo=" + concatenar("Antonio", "Pedreira", "Neiva"));

		String[] nomes = { "Maria", "Pedro", "Jose", "Joaquim" };

		String[] nomes1 = new String[] { "Maria", "Pedro", "Jose", "Joaquim" };

		String[] nomes2 = new String[4];
		nomes2[0] = "Maria";
		nomes2[1] = "Pedro";
		nomes2[2] = "Jose";
		nomes2[3] = "Joaquim";

		System.out.println("\nNomes:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
