package br.ucsal.bes20202.poo.aula06;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAO {

	// Você não deve definir o atributo "fechando" na implementação.
	//private static ArrayList<Aluno> alunos = new ArrayList<>();
	
	private static List<Aluno> alunos = new ArrayList<>();

	/**
	 * Armazenar o aluno passado como parâmetro
	 * 
	 * @param aluno aluno que será armazenado
	 */
	public static void salvar(Aluno aluno) {
		alunos.add(aluno);
	}

	public static Aluno consultar(Integer matricula) {
		// retornar a instância do aluno correspondente à matrícula que foi passada como
		// parâmetro.
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		return null;
	}

	public static Aluno obterPorPosicao(Integer posicao) {
		return alunos.get(posicao);
	}

	public static void excluir(Integer matricula) {
		// excluir o aluno correspondente à matrícula que foi passada como parâmetro.
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				alunos.remove(aluno);
			}
		}
	}

	public static int size() {
		// retornar a quantidade de alunos armazenados.
		return alunos.size();
	}

}
