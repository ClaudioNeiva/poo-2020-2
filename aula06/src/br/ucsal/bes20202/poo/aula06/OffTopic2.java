package br.ucsal.bes20202.poo.aula06;

import java.io.IOException;
import java.util.Scanner;

/**
 * POC: problema do scanner ao realizar a entrada de dados para números e
 * textos.
 * 
 * @author claudioneiva
 *
 */
public class OffTopic2 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		String nome;
		String sobrenome;
		Integer anoNascimento;

		System.out.println("Informe seu nome:");
		nome = scanner.nextLine();

		System.out.println("Informe seu ano de nascimento:");
		anoNascimento = scanner.nextInt();
		scanner.nextLine();

		System.out.println("Informe seu sobrenome:");
		sobrenome = scanner.nextLine();

		System.out.println("Nome=" + nome);
		System.out.println("Sobrenome=" + sobrenome);
		System.out.println("Ano de nascimento=" + anoNascimento);
	}

}
