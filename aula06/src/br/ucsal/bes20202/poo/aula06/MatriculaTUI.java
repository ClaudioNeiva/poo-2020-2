package br.ucsal.bes20202.poo.aula06;

public class MatriculaTUI {

	public static void main(String[] args) {
		cadastrarAluno();
		consultarAluno();
	}

	private static void cadastrarAluno() {
		AlunoArrayDAO.salvar(new Aluno("Claudio Neiva", "claudio.neiva@ucsal.br"));
		AlunoArrayDAO.salvar(new Aluno("Maria da Silva", "maria.silva@ucsal.br"));
		AlunoArrayDAO.salvar(new Aluno("Pedro dos Santos", "pedro.santos@ucsal.br"));
	}

	private static void consultarAluno() {
		
		Aluno aluno1 = AlunoArrayDAO.consultar(1);
		Aluno aluno2 = AlunoArrayDAO.consultar(2);
		Aluno aluno3 = AlunoArrayDAO.consultar(3);
		
		System.out.println("\naluno1:");
		System.out.println("matricula=" + aluno1.getMatricula());
		System.out.println("nome=" + aluno1.getNome());
		System.out.println("email=" + aluno1.getEmail());

		System.out.println("\naluno2:");
		System.out.println("matricula=" + aluno2.getMatricula());
		System.out.println("nome=" + aluno2.getNome());
		System.out.println("email=" + aluno2.getEmail());

		System.out.println("\naluno3:");
		System.out.println("matricula=" + aluno3.getMatricula());
		System.out.println("nome=" + aluno3.getNome());
		System.out.println("email=" + aluno3.getEmail());


	}
	
	private static void cadastrarAluno1() {
		// obter dados do aluno (interface: System.out.print + scanner.nextLine)
		String nome1 = "Claudio Neiva";
		String email1 = "claudio.neiva@ucsal.br";

		String nome2 = "Maria da Silva";

		String email3 = "joao.santos@ucsal.br";

		// instanciar aluno
		// Aluno() = construtor
		Aluno aluno1 = new Aluno(nome1, email1);
		System.out.println("aluno1:");
		aluno1.setNome("Guilherme Bacelar");
		System.out.println("nome=" + aluno1.getNome());
		System.out.println("email=" + aluno1.getEmail());

		Aluno aluno2 = new Aluno("Maria da Silva");
		System.out.println("\n\naluno2:");
		System.out.println("nome=" + aluno2.getNome());
		System.out.println("email=" + aluno2.getEmail());

		Aluno aluno3 = new Aluno("joao.santos@ucsal.br");

		Aluno aluno4 = new Aluno("Antonio", 2000);

	}

}
