package br.ucsal.bes20202.poo.aula06;

public class AlunoArrayDAO {

	private static final int QTD_MAX_ALUNOS = 100;

	private static Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];
	private static int qtd = 0;

	/**
	 * Armazenar o aluno passado como parâmetro
	 * 
	 * @param aluno aluno que será armazenado
	 */
	public static void salvar(Aluno aluno) {
		alunos[qtd] = aluno;
		qtd++;
	}

	public static Aluno consultar(Integer matricula) {
		// retornar a instância do aluno correspondente à matrícula que foi passada como
		// parâmetro.
		for (int i = 0; i < qtd; i++) {
			if (alunos[i] != null && alunos[i].getMatricula().equals(matricula)) {
				return alunos[i];
			}
		}
		return null;
	}

	public static Aluno obterPorPosicao(Integer posicao) {
		return alunos[posicao];
	}

	public static void excluir(Integer matricula) {
		// excluir o aluno correspondente à matrícula que foi passada como parâmetro.
		for (int i = 0; i < qtd; i++) {
			if (alunos[i].getMatricula().equals(matricula)) {
				// FIXME essa abordagem está fazendo com que posições do vetor estejam sendo
				// "perdidas".
				alunos[i] = null;
			}
		}
	}

	public static int size() {
		// retornar a quantidade de alunos armazenados.
		return qtd;
	}

}
