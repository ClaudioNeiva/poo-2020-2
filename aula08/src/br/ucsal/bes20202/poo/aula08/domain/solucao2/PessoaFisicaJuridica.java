package br.ucsal.bes20202.poo.aula08.domain.solucao2;

import br.ucsal.bes20202.poo.aula08.domain.Telefone;

public class PessoaFisicaJuridica {

	String cpf;

	String nome;

	Telefone telefone;

	String nomeMae;

	Integer anoNascimento;

	String cnpj;

	String inscricaoEstadual;

	String inscricaoMunicipal;

	String logradouro;

	String numero;

	String bairro;

	// Construtor da pessoa física
	public PessoaFisicaJuridica(String cpf, String nome, Telefone telefone, String nomeMae, Integer anoNascimento) {
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
		this.nomeMae = nomeMae;
		this.anoNascimento = anoNascimento;
	}

	// Construtor da pessoa jurídica
	public PessoaFisicaJuridica(String cnpj, String nome, Telefone telefone, String inscricaoEstadual,
			String inscricaoMunicipal) {
		this.cnpj = cnpj;
		this.nome = nome;
		this.telefone = telefone;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

}
