package br.ucsal.bes20202.poo.aula08.domain.solucao3;

import java.util.List;

public class Medico extends PessoaFisica {

	private String cmr;

	private List<String> especialidades;

	private List<PessoaFisica> pacientes;

	public String getCmr() {
		return cmr;
	}

	public void setCmr(String cmr) {
		this.cmr = cmr;
	}

	public List<String> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<String> especialidades) {
		this.especialidades = especialidades;
	}

}
