package br.ucsal.bes20202.poo.aula08.domain.solucao3;

import java.util.ArrayList;

public class Exemplo {

	public static void main(String[] args) {

		Medico medico1 = new Medico();

		medico1.nome = "Maria";
		//medico1.telefone = "71112233445";

		medico1.cpf = "99822311433";
		medico1.anoNascimento = 1995;
		medico1.nomeMae = "Ana";

		medico1.setCmr("1233");
		ArrayList<String> especialidades = new ArrayList<String>();
		especialidades.add("Cardiologista");
		especialidades.add("Pneumologista");
		medico1.setEspecialidades(especialidades);

		System.out.println(medico1.descrever());

		// Não é possível instanciar classes que possuem o modificador abstract, ou
		// seja, não é possível instanciar classes abstratas.
		// Pessoa pessoa = new Pessoa();
		// pessoa.nome = "asdasdsa";
		// pessoa.telefone = "239485734589";

		PessoaFisica pessoaFisica1 = new PessoaFisica();
		pessoaFisica1.nome = "claudio neiva";
		
	}
}
