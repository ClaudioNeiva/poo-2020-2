package br.ucsal.bes20202.poo.aula08.domain.solucao3;

public abstract class Pessoa {

	protected String nome;

	private String telefone;
	
	private String logradouro;
	
	String numero;
	
	public String bairro;

	protected String descrever() {
		return nome + " com telefone " + telefone;
	}
	
	private void fazerAlgumaCoisa() {
		
	}

	int fazerOutraCoisa() {
		return 1;
	}
	
	public int fazerOutraOutraCoisa() {
		return 1;
	}
	
}
