package br.ucsal.bes20202.poo.aula08.tui;

import java.util.Scanner;

public class MenuTui {

	private static Scanner scanner = new Scanner(System.in);

	public static void executar() {
		MenuItemEnum opcao;
		do {
			exibir();
			opcao = obterOpcao();
			executarOpcao(opcao);
		} while (opcao != MenuItemEnum.SAIR);
	}

	private static void executarOpcao(MenuItemEnum opcao) {
		switch (opcao) {
		case CADASTRAR:
			ClienteTui.cadastrar();
			break;
		case REMOVER:
			ClienteTui.remover();
			break;
		case PESQUISAR:
			ClienteTui.pesquisar();
			break;
		case LISTAR:
			ClienteTui.listar();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		}
	}

	private static MenuItemEnum obterOpcao() {
		MenuItemEnum opcao;
		String opcaoString;
		while (true) {
			System.out.println("Informe a opção desejada:");
			opcaoString = scanner.nextLine();
			try {
				opcao = MenuItemEnum.valueOf(opcaoString.toUpperCase());
				return opcao;
			} catch (IllegalArgumentException e) {
				System.out.println("Opção inválida!");
			}
		}
	}

	private static void exibir() {
		System.out.println("\n***********SISTEMA CLIENTES ***********\n");
		for (MenuItemEnum menuItem : MenuItemEnum.values()) {
			System.out.println(menuItem);
		}
	}

}
