package br.ucsal.bes20202.poo.aula08.tui;

public enum MenuItemEnum {

	// 1 - Cadastrar
	// 2 - Remover
	// 3 - Pesquisar
	// 4 - Listar clientes ativos
	// 9 - Sair

	// CADASTRAR(1, "Cadastrar"), ..., LISTAR(4, "Listar clientes ativos"), SAIR(9,
	// "Sair")
	// definir atributos para os itens de enumeração

	CADASTRAR, REMOVER, PESQUISAR, LISTAR, SAIR;

}
