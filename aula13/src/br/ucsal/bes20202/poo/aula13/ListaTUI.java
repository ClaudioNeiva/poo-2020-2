package br.ucsal.bes20202.poo.aula13;

public class ListaTUI {

	private ListaTUI() {
	}
	
	/**
	 * 
	 * Exibir todos os elementos que estão na lista passada como parâmetro.
	 * 
	 * @param lista a lista e itens a serem exibidos.
	 */
	public static void exibir(Lista<?> lista) {
		System.out.println("Os itens que estão na lista são:");
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
	}

}
