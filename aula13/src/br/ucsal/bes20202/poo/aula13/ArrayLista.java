package br.ucsal.bes20202.poo.aula13;

public class ArrayLista<T> implements Lista<T> {

	private static final int QTY_MAX = 100;

	private Object[] objects = new Object[QTY_MAX];

	private int size = 0;

	@Override
	public void add(T object) {
		objects[size] = object; // Up cast
		size++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {
		if (index >= 0 && index < size) {
			return (T) objects[index]; // Down cast
		}
		return null;
	}

	@Override
	public int size() {
		return size;
	}

}
