package br.ucsal.bes20202.poo.aula13;

/**
 * Gerenciar uma lista de objetos.
 * 
 * @author claudioneiva
 *
 */
public interface Lista<T> {

	/**
	 * 
	 * Adiciona o objeto passado como parâmetro ao final da lista.
	 * 
	 * @param object o objeto que será adicionado
	 */
	void add(T object);

	/**
	 * 
	 * Recuperar o objeto da posição passada como parâmetro.
	 * 
	 * A posição inicial é a ZERO e posição final é a quantidade de itens da lista
	 * menos um.
	 * 
	 * Se a posição não for válida, então este método deverá retornar null.
	 * 
	 * @param index é a posição do objeto que está sendo pesquisado.
	 * @return o objeto, caso a posição seja válida ou null caso contrário.
	 */
	T get(int index);

	/**
	 * Retornar o tamanho atual da lista
	 */
	int size();

	/**
	 * Recuperar o primeiro objeto da lista. Caso a lista esteja vazia, retorna
	 * null.
	 * 
	 * @return o primeiro objeto da lista ou null (caso a lista esteja vazia).
	 */
	default T getFirst() {
		return get(0);
	}

	/**
	 * Recuperar o último objeto da lista. Caso a lista esteja vazia, retorna null.
	 * 
	 * @return o último objeto da lista ou null (caso a lista esteja vazia).
	 */
	default T getLast() {
		return get(size() - 1);
	}

}
