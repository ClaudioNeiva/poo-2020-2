package br.ucsal.bes20202.poo.atividade02.domain;

//BigDecimal???

public class Veiculo {

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	private Pessoa proprietario;

	public Veiculo(String placa, Integer anoFabricacao, Double valor) {
		definirPlaca(placa);
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
	}

	public void definirPlaca(String placa) {
		validarPlaca(placa);
		this.placa = placa;
	}

	private void validarPlaca(String placa) {
		if (placa == null || placa.trim().length() < 7) {
			throw new RuntimeException("Placa inválida!");
		}
	}

	public void definirAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public void definirValor(Double valor) {
		this.valor = valor;
	}

}
