package br.ucsal.bes20202.poo.atividade02.tui;

import java.util.Scanner;

import br.ucsal.bes20202.poo.atividade02.domain.Veiculo;

public class VeiculoTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void incluir() {
		String placa;
		Integer anoFabricacao;
		Double valor;

		System.out.println("Informe a placa:");
		placa = scanner.nextLine();

		System.out.println("Informe o ano fabricação:");
		anoFabricacao = scanner.nextInt();

		System.out.println("Informe o valor:");
		valor = scanner.nextDouble();

		//Veiculo veiculo = new Veiculo();
		//veiculo.definirPlaca(placa);
		//veiculo.definirAnoFabricacao(anoFabricacao);
		//veiculo.definirValor(valor);

		Veiculo veiculo = new Veiculo(placa, anoFabricacao, valor); 
		
	}

}
