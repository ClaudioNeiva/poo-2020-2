package br.ucsal.bes20202.poo.aula12.polimorfismo;

public class ContaCorrente {

	private Integer numero;

	private String nomeCorrentista;

	protected Double saldo = 0d;

	public ContaCorrente(Integer numero, String nomeCorrentista) {
		super();
		this.numero = numero;
		this.nomeCorrentista = nomeCorrentista;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public Boolean sacar(Double valor) {
		if (valor <= saldo) {
			saldo -= valor;
			return true;
		}
		return false;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public Double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaCorrente [numero=" + numero + ", nomeCorrentista=" + nomeCorrentista + ", saldo=" + saldo + "]";
	}

}
