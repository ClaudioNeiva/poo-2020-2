package br.ucsal.bes20202.poo.aula12.polimorfismo;

public class ContaEspecial extends ContaCorrente {

	private Double limiteCredito;

	public ContaEspecial(Integer numero, String nomeCorrentista, Double limiteCredito) {
		super(numero, nomeCorrentista);
		this.limiteCredito = limiteCredito;
	}
	
	@Override
	public Boolean sacar(Double valor) {
		if(valor <= (saldo + limiteCredito)) {
			saldo -= valor;
			return true;
		}
		return false;
	}

	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	@Override
	public String toString() {
		return "ContaEspecial [limiteCredito=" + limiteCredito + ", toString()=" + super.toString() + "]";
	}


}
