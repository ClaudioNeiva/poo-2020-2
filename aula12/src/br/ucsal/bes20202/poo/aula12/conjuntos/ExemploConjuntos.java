package br.ucsal.bes20202.poo.aula12.conjuntos;

import java.util.HashSet;
import java.util.Set;

public class ExemploConjuntos {

	public static void main(String[] args) {

		Set<Aluno> alunos = new HashSet<>();

		Aluno aluno1 = new Aluno(23, "Antonio");
		Aluno aluno2 = new Aluno(56, "Claudio");
		Aluno aluno3 = new Aluno(67, "Neiva");
		Aluno aluno4 = new Aluno(23, "Antonio");

		System.out.println("aluno1.hashcode=" + aluno1.hashCode());
		System.out.println("aluno2.hashcode=" + aluno2.hashCode());
		System.out.println("aluno3.hashcode=" + aluno3.hashCode());
		System.out.println("aluno4.hashcode=" + aluno4.hashCode());

		alunos.add(aluno1); // 817949718
		alunos.add(aluno2); // -1776645258
		alunos.add(aluno3); // 75151227
		alunos.add(aluno4); // Não vai armazenar, pois tem o mesmo hashcode e equals

		aluno1.setNome("Maria");
		System.out.println("aluno1.hashcode(depois que mudou o nome para Maria)=" + aluno1.hashCode());

		Aluno aluno5 = new Aluno(45, "Joaquim"); // aluno5.hashCode = 817949718
				
		if (alunos.contains(aluno5)) {
			System.out.println("O conjunto contém aluno1");
		} else {
			System.out.println("O conjunto NÃO contém aluno1");
		}

		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

	}

}
