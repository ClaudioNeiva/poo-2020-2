package br.ucsal.bes20202.poo.aula10.revenda;

public class VeiculoPesado extends VeiculoCarga {

	public VeiculoPesado(String placa, Integer anoFabricacao, Double valor, Pessoa comprador, Integer capacidadeCarga,
			Integer quantidadeEixos, Integer capacidadeTanqueCombustivel) {
		super(placa, anoFabricacao, valor, comprador, capacidadeCarga, quantidadeEixos, capacidadeTanqueCombustivel);
	}
	
	@Override
	public String descrever() {
		return " método descrever para veículos pesados";
	}

}
