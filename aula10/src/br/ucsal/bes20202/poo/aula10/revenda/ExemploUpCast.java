package br.ucsal.bes20202.poo.aula10.revenda;

public class ExemploUpCast {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa("252525252552", "Claudio Neiva");

		Moto moto1 = new Moto("ABC12F3", 2010, 15000.23, pessoa1, CategoriaMotoEnum.ESTRADA, 50);
		VeiculoCarga veiculoCarga1 = new VeiculoCarga("OPI1I23", 2015, 234000.45, pessoa1, 15, 8, 350);
		VeiculoPasseio veiculoPasseio1 = new VeiculoPasseio("ABC4455", 2000, 50000d, pessoa1, 5, 450);

		Veiculo[] veiculos = new Veiculo[4];
		veiculos[0] = moto1;
		veiculos[1] = veiculoCarga1;
		veiculos[2] = veiculoPasseio1;
		veiculos[3] = new VeiculoPesado("DDF2344", 2010, 150000d, pessoa1, 15, 8, 350);
		gerarRelatorio(veiculos);
	}

	private static void gerarRelatorio(Veiculo[] veiculos) {
		System.out.println("Gerar relatório:");
		for (Veiculo veiculo : veiculos) {
			System.out.println("classe=" + veiculo.getClass());
			System.out.println("\t" + veiculo.descrever() + "\n");
		}
	}

}
