package br.ucsal.bes20202.poo.aula10.revenda;

public class ExemploUpCastDownCast {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa("252525252552", "Claudio Neiva");

		Moto moto1 = new Moto("ABC12F3", 2010, 15000.23, pessoa1, CategoriaMotoEnum.ESTRADA, 50);
		VeiculoCarga veiculoCarga1 = new VeiculoCarga("OPI1I23", 2015, 234000.45, pessoa1, 15, 8, 350);
		VeiculoPasseio veiculoPasseio1 = new VeiculoPasseio("ABC4455", 2000, 50000d, pessoa1, 5, 450);

		// Up cast (pode ser implícito ou explícito)
		// Veiculo veiculo1 = (Veiculo) moto1;
		Veiculo veiculo1 = moto1;
		veiculo1.setPlaca("WER3388");
		System.out.println("placa da moto(consultado via veiculo1)=" + veiculo1.getPlaca());
		// NÃO é possível chamar métodos da instância apontada por moto1 através da
		// variável local veiculo1, pois o datatype de veiculo1 é Veiculo.
		// veiculo1.setCategoria(CategoriaMotoEnum.TRILHA);

		// Down cast (deve ser explícito)
		if (veiculo1 instanceof Moto) {
			Moto motoA = (Moto) veiculo1;
			motoA.setCategoria(CategoriaMotoEnum.TRILHA);
			System.out.println("categoria da moto(consultada via motoA)=" + motoA.getCategoria());
		}

		Veiculo veiculo2 = veiculoCarga1;

		VeiculoCarga veiculoCargaA = (VeiculoCarga) veiculo2;

		if (veiculo2 instanceof Moto) {
			Moto motoB = (Moto) veiculo2;
			motoB.setCategoria(CategoriaMotoEnum.URBANA);
		} else {
			System.out.println(
					"como veiculo2 não aponta para um Moto (ou subclasse de Moto, caso exista), então não possível fazer o downcast.");
		}
		Veiculo[] veiculos = new Veiculo[3];

		// Atribuição de uma variável da subclasse para uma variáveis cujo tipo é uma
		// superclasse dessa variável.
		// Up cast
		veiculos[0] = moto1;

		veiculos[1] = veiculoCarga1;
		veiculos[2] = veiculoPasseio1;
		gerarRelatorio(veiculos);
	}

	private static void gerarRelatorio(Veiculo[] veiculos) {
		System.out.println("Gerar relatório:");
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo.descrever());
		}
	}

}
