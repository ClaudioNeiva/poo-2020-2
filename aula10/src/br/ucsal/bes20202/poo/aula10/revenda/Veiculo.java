package br.ucsal.bes20202.poo.aula10.revenda;

public class Veiculo {

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	private Pessoa comprador;

	public Veiculo(String placa, Integer anoFabricacao, Double valor, Pessoa comprador) {
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
		this.comprador = comprador;
	}

	public String descrever() {
		return "Placa " + placa + " fabricado em " + anoFabricacao + " vendido por " + valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor + ", comprador="
				+ comprador + "]";
	}

}
