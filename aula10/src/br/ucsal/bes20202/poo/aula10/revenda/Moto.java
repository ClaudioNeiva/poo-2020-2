package br.ucsal.bes20202.poo.aula10.revenda;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer quantidadeCilindradas;

	public Moto(String placa, Integer anoFabricacao, Double valor, Pessoa comprador, CategoriaMotoEnum categoria,
			Integer quantidadeCilindradas) {
		super(placa, anoFabricacao, valor, comprador);
		// System.out.println("executou a instanciação do moto...");
		this.categoria = categoria;
		this.quantidadeCilindradas = quantidadeCilindradas;
	}

	@Override
	public String descrever() {
		return super.descrever() + " da categoria " + categoria + " com " + quantidadeCilindradas + " cilindradas.";
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQuantidadeCilindradas() {
		return quantidadeCilindradas;
	}

	public void setQuantidadeCilindradas(Integer quantidadeCilindradas) {
		this.quantidadeCilindradas = quantidadeCilindradas;
	}

}
