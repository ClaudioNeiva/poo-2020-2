package br.ucsal.bes20202.poo.aula05.exemplo1.business;

import br.ucsal.bes20202.poo.aula05.exemplo1.domain.ContaCorrente;
import br.ucsal.bes20202.poo.aula05.exemplo1.persistence.ContaCorrenteDAO;

//TODO Não é recomendável trabalhar com o mesmo identificador em diferentes pacotes. Por exemplo, Banco no pacote tui e Banco no pacote business. 
public class Banco {

	public static void registrarContaCorrente(ContaCorrente contaCorrente) throws Exception {
		// Validar a contaCorrente
		if (contaCorrente.nomeCorrentista.length() < 10) {
			// O nome do correntista não é valido!!!!
			throw new Exception("Nome do correntista inválido! O nome deve ter no mínimo 10 caracters.");
		}
		ContaCorrenteDAO.salvar(contaCorrente);
	}

}
