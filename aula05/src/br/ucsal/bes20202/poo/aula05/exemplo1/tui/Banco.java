package br.ucsal.bes20202.poo.aula05.exemplo1.tui;

import java.util.Scanner;

import br.ucsal.bes20202.poo.aula05.exemplo1.domain.ContaCorrente;

//TODO Não é recomendável trabalhar com o mesmo identificador em diferentes pacotes. Por exemplo, Banco no pacote tui e Banco no pacote business. 
public class Banco {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		cadastrarContaCorrente();
	}

	private static void cadastrarContaCorrente() {
		Integer numeroConta;
		String nomeCorrentista;
		System.out.println("Informar o número da conta:");
		numeroConta = scanner.nextInt();
		System.out.println("Informar o nome do correntista:");
		nomeCorrentista = scanner.nextLine();

		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.numero = numeroConta;
		contaCorrente.nomeCorrentista = nomeCorrentista;

		try {
			br.ucsal.bes20202.poo.aula05.exemplo1.business.Banco.registrarContaCorrente(contaCorrente);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
