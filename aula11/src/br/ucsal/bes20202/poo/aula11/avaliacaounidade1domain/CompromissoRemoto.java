package br.ucsal.bes20202.poo.aula11.avaliacaounidade1domain;

import java.util.List;

public class CompromissoRemoto extends Compromisso {

	private String aplicativo;

	private String url;

	public CompromissoRemoto(String descricao, String dataHora, Integer duracao, List<Pessoa> participantes,
			TipoAvisoEnum tipoAviso, String aplicativo, String url) {
		super(descricao, dataHora, duracao, participantes, tipoAviso);
		this.aplicativo = aplicativo;
		this.url = url;
	}

	public String getAplicativo() {
		return aplicativo;
	}

	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "CompromissoRemoto [aplicativo=" + aplicativo + ", url=" + url + ", toString()=" + super.toString()
				+ "]";
	}

}
