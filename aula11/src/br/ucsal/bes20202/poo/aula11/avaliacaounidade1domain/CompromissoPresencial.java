package br.ucsal.bes20202.poo.aula11.avaliacaounidade1domain;

import java.util.List;

public class CompromissoPresencial extends Compromisso {

	private String endereco;

	public CompromissoPresencial(String descricao, String dataHora, Integer duracao, List<Pessoa> participantes,
			TipoAvisoEnum tipoAviso, String endereco) {
		super(descricao, dataHora, duracao, participantes, tipoAviso);
		this.endereco = endereco;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "CompromissoPresencial [endereco=" + endereco + ", toString()=" + super.toString() + "]";
	}

}
