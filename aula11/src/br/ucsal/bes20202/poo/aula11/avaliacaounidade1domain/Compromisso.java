package br.ucsal.bes20202.poo.aula11.avaliacaounidade1domain;

import java.util.List;

public class Compromisso {

	private String descricao;

	private String dataHora;

	private Integer duracao;

	private List<Pessoa> participantes;

	private TipoAvisoEnum tipoAviso;

	public Compromisso(String descricao, String dataHora, Integer duracao, List<Pessoa> participantes,
			TipoAvisoEnum tipoAviso) {
		super();
		this.descricao = descricao;
		this.dataHora = dataHora;
		this.duracao = duracao;
		this.participantes = participantes;
		this.tipoAviso = tipoAviso;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	public Integer getDuracao() {
		return duracao;
	}

	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}

	public List<Pessoa> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<Pessoa> participantes) {
		this.participantes = participantes;
	}

	public TipoAvisoEnum getTipoAviso() {
		return tipoAviso;
	}

	public void setTipoAviso(TipoAvisoEnum tipoAviso) {
		this.tipoAviso = tipoAviso;
	}

	@Override
	public String toString() {
		return "Compromisso [descricao=" + descricao + ", dataHora=" + dataHora + ", duracao=" + duracao
				+ ", participantes=" + participantes + ", tipoAviso=" + tipoAviso + "]";
	}

}
