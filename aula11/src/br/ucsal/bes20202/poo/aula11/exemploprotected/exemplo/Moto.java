package br.ucsal.bes20202.poo.aula11.exemploprotected.exemplo;

import br.ucsal.bes20202.poo.aula11.exemploprotected.domain.CategoriaMotoEnum;
import br.ucsal.bes20202.poo.aula11.exemploprotected.domain.Pessoa;
import br.ucsal.bes20202.poo.aula11.exemploprotected.domain.Veiculo;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer quantidadeCilindradas;
	@Override
	public String descrever() {
		return "placa=" + placa + " da categoria " + categoria + " com " + quantidadeCilindradas + " cilindradas.";
	}

	public Moto(String placa, Integer anoFabricacao, Double valor, Pessoa comprador, CategoriaMotoEnum categoria,
			Integer quantidadeCilindradas) {
		super(placa, anoFabricacao, valor, comprador);
		// System.out.println("executou a instanciação do moto...");
		this.categoria = categoria;
		this.quantidadeCilindradas = quantidadeCilindradas;
	}


	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQuantidadeCilindradas() {
		return quantidadeCilindradas;
	}

	public void setQuantidadeCilindradas(Integer quantidadeCilindradas) {
		this.quantidadeCilindradas = quantidadeCilindradas;
	}

}
