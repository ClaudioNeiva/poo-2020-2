package br.ucsal.bes20202.poo.aula11.exemploprotected.domain;

public enum CategoriaMotoEnum {

	URBANA, TRILHA, ESTRADA;

}
