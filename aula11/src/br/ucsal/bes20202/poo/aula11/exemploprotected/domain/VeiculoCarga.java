package br.ucsal.bes20202.poo.aula11.exemploprotected.domain;

public class VeiculoCarga extends Veiculo {

	private Integer capacidadeCarga;

	private Integer quantidadeEixos;

	private Integer capacidadeTanqueCombustivel;

	public VeiculoCarga(String placa, Integer anoFabricacao, Double valor, Pessoa comprador, Integer capacidadeCarga,
			Integer quantidadeEixos, Integer capacidadeTanqueCombustivel) {
		super(placa, anoFabricacao, valor, comprador);
		this.capacidadeCarga = capacidadeCarga;
		this.quantidadeEixos = quantidadeEixos;
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

	@Override
	public String descrever() {
		return " tem " + quantidadeEixos + " eixos, com capacidade de carga de " + capacidadeCarga
				+ " toneladas e um tanque de " + capacidadeTanqueCombustivel + " litros.";
	}

	public Integer getCapacidadeCarga() {
		return capacidadeCarga;
	}

	public void setCapacidadeCarga(Integer capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}

	public Integer getQuantidadeEixos() {
		return quantidadeEixos;
	}

	public void setQuantidadeEixos(Integer quantidadeEixos) {
		this.quantidadeEixos = quantidadeEixos;
	}

	public Integer getCapacidadeTanqueCombustivel() {
		return capacidadeTanqueCombustivel;
	}

	public void setCapacidadeTanqueCombustivel(Integer capacidadeTanqueCombustivel) {
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

}
