package br.ucsal.bes20202.poo.aula11.conjuntos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ExemploConjuntos {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		// List<String> nomes1 = obter5NomesDistintosUsandoList();
		Set<String> nomes2 = obter5NomesDistintosUsandoSet();
		// exibirNomes(nomes1);
		exibirNomes(nomes2);
	}

	/**
	 * Solicitar do usuário uma quantidade não predeterminada de nomes, e vai parar
	 * de pedir quando o usuário tiver informado 5 nomes diferentes e retornar essa
	 * lista.
	 * 
	 * @return lista de nomes distintos
	 */
	private static List<String> obter5NomesDistintosUsandoList() {
		List<String> nomes = new ArrayList<>();
		System.out.println("Informe 5 nomes distintos:");
		do {
			String nome = scanner.nextLine();
			if (!nomes.contains(nome)) {
				nomes.add(nome);
			}
		} while (nomes.size() < 5);
		return nomes;
	}

	private static Set<String> obter5NomesDistintosUsandoSet() {
		Set<String> nomes = new HashSet<>();
		// Set<String> nomes = new LinkedHashSet<>();
		// Set<String> nomes = new TreeSet<>();
		System.out.println("Informe 5 nomes distintos:");
		do {
			String nome = scanner.nextLine();
			nomes.add(nome);
		} while (nomes.size() < 5);
		return nomes;
	}

	/**
	 * Exibir os nomes, um por linha.
	 * 
	 * @param nomes - nomes a serem listados
	 */
	private static void exibirNomes(Collection<String> nomes) {
		System.out.println("Nomes informados:");
//		for (String nome : nomes) {
//			System.out.println(nome);
//		}
		nomes.stream().forEach(System.out::println);
	}
}
