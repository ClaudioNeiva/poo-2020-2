package br.ucsal.bes20202.poo.aula11.mapas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ExemploMapa {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		List<Integer> numeros = obter10Numeros();
		Map<Integer, Integer> numerosQtdRepeticoes = calcularQtdRepeticoes(numeros);
		exibirMapa(numerosQtdRepeticoes);
	}

	private static void exibirMapa(Map<Integer, Integer> numerosQtdRepeticoes) {
		System.out.println("Número x qtd de repetiçães:");
		for (Integer n : numerosQtdRepeticoes.keySet()) {
			System.out.println(n + " x " + numerosQtdRepeticoes.get(n));
		}
	}

	/**
	 * Exemplo:
	 * 
	 * Entrada: 3, 5, 1, 3, 5, 5, 5, 8, 2, 1
	 * 
	 * Saída: 3 -> 2x, 5 -> 4x, 1 -> 2x, 2 -> 1x, 8 -> 1x
	 * 
	 * Map<key, value> mapa (key é um conjunto (Set))
	 * 
	 * mapa.put(key, value)
	 * 
	 * mapa.get(key) -> retorna o valor
	 * 
	 * key | value
	 * 
	 * 3 | 2
	 * 
	 * 5 | 4
	 * 
	 * 1 | 2
	 * 
	 * 2 | 1
	 * 
	 * 8 | 1
	 * 
	 * @param numeros
	 * @return
	 */
	private static Map<Integer, Integer> calcularQtdRepeticoes(List<Integer> numeros) {
		// Map<key=numero, value=quatidade de vezes que o número foi informado>
		Map<Integer, Integer> numerosQtds = new HashMap<>();
		for (Integer n : numeros) {
			if (numerosQtds.containsKey(n)) {
				Integer qtdAtual = numerosQtds.get(n);
				numerosQtds.put(n, qtdAtual + 1);
			} else {
				numerosQtds.put(n, 1);
			}
		}
		return numerosQtds;
	}

	private static List<Integer> obter10Numeros() {
		List<Integer> numeros = new ArrayList<>();
		System.out.println("Informe 10 números:");
		for (int i = 0; i < 10; i++) {
			Integer n = scanner.nextInt();
			numeros.add(n);
			// numeros.add(scanner.nextInt());
		}
		return numeros;
	}

}
