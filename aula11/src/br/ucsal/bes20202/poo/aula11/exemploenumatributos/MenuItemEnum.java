package br.ucsal.bes20202.poo.aula11.exemploenumatributos;

public enum MenuItemEnum {

	CADASTRAR(1, "Cadastrar"),

	REMOVER(2, "Remover"),

	PESQUISAR(3, "Pesquisar"),

	LISTAR(4, "Listar clientes ativos"),

	ATUALIZAR(5, "Atualizar"),

	SAIR(9, "Sair");

	private Integer codigo;
	private String descricao;

	private MenuItemEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static MenuItemEnum valueOfCodigo(Integer codigoPesquisa) {
		for (MenuItemEnum menuItem : values()) {
			if (codigoPesquisa.equals(menuItem.codigo)) {
				return menuItem;
			}
		}
		throw new IllegalArgumentException("O código " + codigoPesquisa + " não existe na enumeração");
	}

}
