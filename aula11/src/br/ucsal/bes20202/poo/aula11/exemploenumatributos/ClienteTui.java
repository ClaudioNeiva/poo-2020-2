package br.ucsal.bes20202.poo.aula11.exemploenumatributos;

public class ClienteTui {

	public static void cadastrar() {
		System.out.println("Cadastrar cliente...");
	}

	public static void remover() {
		System.out.println("Remover cliente...");
	}

	public static void pesquisar() {
		System.out.println("Pesquisar cliente...");
	}

	public static void listar() {
		System.out.println("Listar cliente...");
	}

}
