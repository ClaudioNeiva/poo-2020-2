package br.ucsal.bes20202.poo.aula11.exemploenumatributos;

import java.util.Scanner;

public class MenuTudoEmUmMetodo {

	// 1 - Cadastrar
	// 2 - Remover
	// 3 - Pesquisar
	// 4 - Listar
	// 9 - Sair

	private static Scanner scanner = new Scanner(System.in);

	// Este método tem uma COESÃO ruim (coesão sequencial)
	public static void executar() {
		int opcao;
		do {
			System.out.println("\n\n");
			System.out.println("1 - Cadastar");
			System.out.println("2 - Remover");
			System.out.println("3 - Pesquisar");
			System.out.println("4 - Listar");
			System.out.println("9 - Sair");
			do {
				System.out.println("Informe a opção desejada:");
				opcao = scanner.nextInt();
				if (opcao != 1 && opcao != 2 && opcao != 3 && opcao != 4 && opcao != 9) {
					System.out.println("Opção inválida.");
				}
			} while (opcao != 1 && opcao != 2 && opcao != 3 && opcao != 4 && opcao != 9);
			switch (opcao) {
			case 1:
				ClienteTui.cadastrar(); 
				break;
			case 2:
				ClienteTui.remover(); 
				break;
			case 3:
				ClienteTui.pesquisar(); 
				break;
			case 4:
				ClienteTui.listar(); 
				break;
			case 9:
				System.out.println("Bye...");
				break;
			default:
				System.out.println("Opção inválida.");
				break;
			}
		} while (opcao != 9);
	}

}
