package br.ucsal.bes20202.poo.aula14.exemplo2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Exemplo2 {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Funcionario("2134234", "Claudio Neiva", LocalDate.of(2000, 11, 23)));
		pessoas.add(new Funcionario("1232222", "Daniel Diniz", LocalDate.of(2010, 5, 12)));

		pessoas.add(new Usuario("4564564", "Maria da Silva", "asd345"));
		pessoas.add(new Usuario("1122334", "Guilherme BAcelar", "utrut6"));

		for (Pessoa pessoa : pessoas) {
			
			if (pessoa instanceof Usuario) {
				
				Usuario usuario = (Usuario) pessoa;
				System.out.println("senha=" + usuario.getSenha());
				
				// Fazendo os comandos anteiores em um único passo
				System.out.println("senha=" + ((Usuario) pessoa).getSenha());
				
			} else if (pessoa instanceof Funcionario) {
				
				Funcionario funcionario = (Funcionario) pessoa;
				System.out.println("data de admissão=" + funcionario.getDataAdmissao());
				
			}
		}

	}

}
