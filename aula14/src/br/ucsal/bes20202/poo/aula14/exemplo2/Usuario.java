package br.ucsal.bes20202.poo.aula14.exemplo2;

public class Usuario extends Pessoa {

	private String senha;

	public Usuario(String cpf, String nome, String senha) {
		super(cpf, nome);
		this.senha = senha;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "Usuario [senha=" + senha + ", toString()=" + super.toString() + "]";
	}

}
