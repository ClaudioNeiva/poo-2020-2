package br.ucsal.bes20202.poo.aula14.exemplo2;

import java.time.LocalDate;

public class Funcionario extends Pessoa {

	private LocalDate dataAdmissao;

	public Funcionario(String cpf, String nome, LocalDate dataAdmissao) {
		super(cpf, nome);
		this.dataAdmissao = dataAdmissao;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	@Override
	public String toString() {
		return "Funcionario [dataAdmissao=" + dataAdmissao + "]";
	}

}
