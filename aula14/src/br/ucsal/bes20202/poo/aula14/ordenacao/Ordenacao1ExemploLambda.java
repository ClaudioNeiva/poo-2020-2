package br.ucsal.bes20202.poo.aula14.ordenacao;

import java.util.Arrays;
import java.util.List;

public class Ordenacao1ExemploLambda {

	public static void main(String[] args) {

		List<String> nomes = Arrays.asList("claudio", "maria", "pedro", "ana", "joão");

		System.out.println("\nOs nomes na ordem que foram inseridos:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nOs nomes na ordem que foram inseridos:");
		nomes.forEach(System.out::println);

		System.out.println("\nOs nomes na ordem que foram inseridos:");
		nomes.forEach(Ordenacao1ExemploLambda::exibir);
}

	public static void exibir(String nome) {
		System.out.println("Bom dia, " + nome+"!");
	}

}
