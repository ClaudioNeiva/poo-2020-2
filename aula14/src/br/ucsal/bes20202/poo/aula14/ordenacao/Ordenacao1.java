package br.ucsal.bes20202.poo.aula14.ordenacao;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Ordenacao1 {

	public static void main(String[] args) {

		List<String> nomes = Arrays.asList("claudio", "maria", "pedro", "ana", "joão", "antonio");

		// C = 67
		// m = 109
		// p = 112
		// a = 97
		// j = 106
		// n = 110
		// t = 116

		System.out.println("\nOs nomes na ordem que foram inseridos:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		// Ordenação usando Java 8
		nomes.sort(Comparator.naturalOrder());
		// Na verdade, a ordem é dos valores do caracteres ASCII.
		System.out.println("\nOs nomes em ordem crescente (para String é ordem 'alfabética'):");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		exibirLetrasCodigosAscii("bom dia para todos!");

	}

	private static void exibirLetrasCodigosAscii(String texto) {
		System.out.println("Texto="+texto);
		for (int i = 0; i < texto.length(); i++) {
			char letra = texto.charAt(i);
			System.out.println(letra + " -> " + (byte) letra);
		}
	}

}
