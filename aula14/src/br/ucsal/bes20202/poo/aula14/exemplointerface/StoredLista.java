package br.ucsal.bes20202.poo.aula14.exemplointerface;

public interface StoredLista<T> extends Lista<T> {

	void save(Integer id);
	
	void load(Integer id);
	
}
