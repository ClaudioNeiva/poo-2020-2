package br.ucsal.bes20202.poo.aula14.exemplointerface;

public class LinkedLista<T> implements Lista<T> {

	private Node start;

	private Node end;

	private Integer size = 0;

	@Override
	public void add(T object) {
		Node newNode = new Node(object);
		if (end == null) {
			end = newNode;
			start = end;
		} else {
			end.prox = newNode;
			end = newNode;
		}
		size++;
	}

	@Override
	public T get(int index) {
		Node aux = start;
		for (int i = 0; i < index; i++) {
			if (aux != null) {
				aux = aux.prox;
			}
		}
		if (aux != null) {
			return aux.element;
		}
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	private class Node {
		private T element;
		private Node prox;

		public Node(T object) {
			element = object;
		}
	}

}
