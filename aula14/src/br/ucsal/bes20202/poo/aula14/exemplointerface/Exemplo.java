package br.ucsal.bes20202.poo.aula14.exemplointerface;

import java.time.LocalDate;

public class Exemplo {

	public static void main(String[] args) {

		Lista<String> nomes = new ArrayLista<>();
		nomes.add("Claudio");
		nomes.add("Antonio");
		nomes.add("Neiva");

		String primeiroNomeLista = nomes.getFirst();
		System.out.println("Primeiro nome da lista: " + primeiroNomeLista);

		ListaTUI.exibir(nomes);

		Lista<Aluno> alunos = new LinkedLista<>();
		alunos.add(new Aluno(12, "Ana"));
		alunos.add(new Aluno(23, "Pedro"));
		alunos.add(new Aluno(67, "Carla"));

		ListaTUI.exibir(alunos);

		ArrayLista<Object> coisas = new ArrayLista<>();
		coisas.add("Claudio");
		coisas.add(1223123);
		coisas.add(LocalDate.of(2020, 11, 13));

		System.out.println("Capacidade máxima da lista de coisas=" + coisas.getMaxCapacity());
		ListaTUI.exibir(coisas);

	}

}
