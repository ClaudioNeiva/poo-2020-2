package br.ucsal.bes20202.poo.aula02.exemplo1;

// Quando uma classe não tem controle de visibilidade de seus atributos,
// não tem métodos, podemos pensar nela então como um tipo de dado
// composto heterogênio.
public class Aluno {

	Integer matricula;

	String nome;

	Integer anoIngresso;

	String email;

	// br.ucsal.bes20202.poo.aula02.exemplo1.Aluno

	public void matricular() {

	}

}

//1970
//1991-> 	AT 286  -> 1 núcleo		   	   16Mhz		640KB	US$2.500
//1994-> 	AT 486  -> 1 núcleo		   	  100Mhz	   2048KB	US$2.000
//1995-> 	surgimento do Java
//1997-> 	K6-2	-> 1 núcleo		   	  500Mhz	   8048KB	US$1.300
//2020->	core i5 -> 4 núcleos		2.100Mhz	8000000Kb	US$  350
