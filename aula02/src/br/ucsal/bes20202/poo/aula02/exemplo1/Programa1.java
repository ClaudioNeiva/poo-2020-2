package br.ucsal.bes20202.poo.aula02.exemplo1;

import java.util.List;

public class Programa1 {

	int numeroContaCorrente;
	String nomeCorrentista;
	double saldoContaCorrente = 0d;
	String nomeAgencia;
	String enderecoAgencia;
	List<String> nomesEmpregados;
	String nomeGerente;
	
	void substituirGerente(String nome) {
		nomeGerente = nome;
	}
	
	void adicionarEmpregado(String nome) {
		nomesEmpregados.add(nome);
	}
	
	void depositar(double valor) {
		saldoContaCorrente += valor;
	}

	void sacar(double valor) {
		saldoContaCorrente -= valor;
	}
	
	double consultarSaldo() {
		return saldoContaCorrente;
	}
	
}
