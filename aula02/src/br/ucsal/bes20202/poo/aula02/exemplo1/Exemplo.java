package br.ucsal.bes20202.poo.aula02.exemplo1;

public class Exemplo {

	public static void main(String[] args) {

		Aluno alunoFilipe = new Aluno();

		alunoFilipe.nome = "Filipe Caldas";
		alunoFilipe.anoIngresso = 2020;
		alunoFilipe.matricula = 1234;
		alunoFilipe.email = "filipe.caldas@ucsal.edu.br";

		Aluno alunoCarlos = new Aluno();

		alunoCarlos.nome = "Carlos Pinho";
		alunoCarlos.anoIngresso = 2020;
		alunoCarlos.matricula = 7567;
		alunoCarlos.email = "carlos.pinho@ucsal.edu.br";

		
		
		apresentarAluno(alunoFilipe);
		apresentarAluno(alunoCarlos);

	}

	static void apresentarAluno(Aluno alunoFilipe) {
		System.out.println("O aluno " + alunoFilipe.nome + " tem as seguintes características:");
		System.out.println("Matrícula: " + alunoFilipe.matricula);
		System.out.println("Ano de ingresso: " + alunoFilipe.anoIngresso);
		System.out.println("E-mail: " + alunoFilipe.email);
	}

}
