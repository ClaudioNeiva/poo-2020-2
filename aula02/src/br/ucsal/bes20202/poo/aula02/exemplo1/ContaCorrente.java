package br.ucsal.bes20202.poo.aula02.exemplo1;

class ContaCorrente {

	int numero;
	
	String nomeCorrentista;
	
	double saldo = 0d;
	
	void depositar(double valor) {
		saldo += valor;
	}

	void sacar(double valor) {
		saldo -= valor;
	}
	
	double consultarSaldo() {
		return saldo;
	}

}

