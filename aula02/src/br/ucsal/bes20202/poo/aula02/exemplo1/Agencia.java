package br.ucsal.bes20202.poo.aula02.exemplo1;

import java.util.List;

class Agencia {

	String nome;
	
	String endereco;
	
	List<String> nomesEmpregados;
	
	String nomeGerente;
	
	void substituirGerente(String nome) {
		nomeGerente = nome;
	}
	
	void adicionarEmpregado(String nome) {
		nomesEmpregados.add(nome);
	}
	
}
