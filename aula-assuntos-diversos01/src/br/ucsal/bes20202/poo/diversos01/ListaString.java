package br.ucsal.bes20202.poo.diversos01;

public interface ListaString {

	void adicionar(String elemento);

	String obterElement(int i);

	int obterTamanho();

}
