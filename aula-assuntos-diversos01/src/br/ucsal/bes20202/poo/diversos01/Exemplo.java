package br.ucsal.bes20202.poo.diversos01;

public class Exemplo {

	public static void main(String[] args) {
		ListaString lista1 = new ArrayListaString();
		lista1.adicionar("antonio");
		lista1.adicionar("claudio");
		lista1.adicionar("neiva");
		System.out.println("Elemento da posição zero: " + lista1.obterElement(0));
		System.out.println("Tamanho da lista:" + lista1.obterTamanho());
	}

}
