package br.ucsal.bes20202.poo.aula02.aula03;

import java.util.Scanner;

public class Exemplo {

	public static void main(String[] args) {

		instanciarDiversosAlunos();

		Aluno alunoFilipe = new Aluno();

		alunoFilipe.nome = "Filipe Caldas";
		alunoFilipe.anoIngresso = 2020;
		alunoFilipe.matricula = 1234;
		alunoFilipe.email = "filipe.caldas@ucsal.edu.br";

		Aluno alunoCarlos = new Aluno();

		alunoCarlos.nome = "Carlos Pinho";
		alunoCarlos.anoIngresso = 2020;
		alunoCarlos.matricula = 7567;
		alunoCarlos.email = "carlos.pinho@ucsal.edu.br";

		apresentarAluno(alunoFilipe);
		apresentarAluno(alunoCarlos);

		System.out.println("main - Tecle ENTER para continuar...");
		new Scanner(System.in).nextLine();

	}

	private static void instanciarDiversosAlunos() {
		Aluno[] alunos = new Aluno[10000000];
		
		for (int i = 0; i < alunos.length; i++) {
			alunos[i] = new Aluno();
			alunos[i].nome = "antonio claudio neiva " + i;
		}
		System.out.println("instanciarDiversosAlunos - Tecle ENTER para continuar...");
		new Scanner(System.in).nextLine();
	}

	static void apresentarAluno(Aluno alunoFilipe) {
		System.out.println("O aluno " + alunoFilipe.nome + " tem as seguintes características:");
		System.out.println("Matrícula: " + alunoFilipe.matricula);
		System.out.println("Ano de ingresso: " + alunoFilipe.anoIngresso);
		System.out.println("E-mail: " + alunoFilipe.email);
	}

}
